<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
<title>智邮宝</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="blank" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="full-screen" content="yes">
<meta name="x5-fullscreen" content="true">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.mobile.css">
<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.css">
<script src="/Public/Common/plugin/layui-v2.2.6/layui/layui.js"></script>

<script src='http://app.lxh.magcloud.cc/public/static/dest/js/libs/magjs-x.js'></script>

<!-- import Vue before Element -->
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<link rel="stylesheet" href="/Public/Common/css/common.css">
<link rel="stylesheet" href="/Public/Home/css/common.css">
</head>
<body class="layui-layout-body">
<ul class="layui-nav layui-bg-blue header">
    <li class="layui-nav-item">
        <a href="javascript:history.go(-1);"><i class="layui-icon">&#xe603;</i></a>
    </li>
    <li class="layui-nav-item title">
        <a href="javascript:;">东营</a>
        <dl class="layui-nav-child">
            <dd><a href="">济南</a></dd>
            <dd><a href="">青岛</a></dd>
            <dd><a href="">烟台</a></dd>
            <dd><a href="">潍坊</a></dd>
            <dd><a href="">威海</a></dd>
        </dl>
    </li>
    <li class="layui-nav-item fr">
        <a href=""><i class="layui-icon">&#xe715;</i></a>
    </li>
</ul>
<script>
    layui.use('element', function () {
        var element = layui.element;

    });
</script>
<style>
    .func .layui-btn {
        width: 50%;
        float: left;
        padding: 0;
        margin: 0;
    }
</style>
<form action="" class="layui-form" method="post">
    <div id="step1">
        <p>门票名称:<?php echo ($info["name"]); ?></p>

        <input type="hidden" name="sub_orderCode" value="<?php echo ($sub_order_code); ?>" placeholder="子订单号" class="layui-input">

        <p>单价: <span id="price"><?php echo ($info["tprice"]); ?> - </span></p>
        <input type="hidden" name="totalPrice" value="<?php echo ($info["price"]); ?>">
        <p>总额: <span id="totalPrice"><?php echo ($info["price"]); ?></span></p>

        <p>数量：<input type="number" name="num" placeholder="数量" class="layui-input" value="1"></p>
        <?php if($info['is_play_date']): ?><p>游玩时间:
                <input type="text" name="playDate" class="layui-input" placeholder="游玩时间">
            </p><?php endif; ?>
        <p>
            <?php if($info['is_real']): ?><div class="layui-btn layui-btn-danger layui-btn-block next">下一步</div>
                <?php else: ?>
                <input type="submit" class="layui-btn layui-btn-danger layui-btn-block" value="付款"><?php endif; ?>
        </p>
        <p>订单金额: <span id="orderPrice"></span></p>
    </div>
    <div id="step2" style="display: none">
        <input type="hidden" name="main_orderCode" value="<?php echo ($order_code); ?>" placeholder="主订单号" class="layui-input">

        <input type="text" name="certificateNo" placeholder="身份证号" class="layui-input">
        <input type="text" name="linkName" placeholder="联系人" class="layui-input">
        <input type="text" name="linkMobile" placeholder="联系电话" class="layui-input">

        <div class="func">
            <div class="layui-btn layui-btn-normal prev">上一步</div>
            <input type="submit" class="layui-btn layui-btn-danger layui-btn-block" value="付款">
        </div>
    </div>
</form>
<script src="/Public/Home/js/pay_is_real.js"></script>
<script>
    layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate;

        laydate.render({
            elem: 'input[name=playDate]'
        });
    })
    // 修改金额
    $(function () {
        $('input[name=num]').change(function () {
            var price = $('span#price').html();
            var total_price = Number($(this).val()) * Number(price);
            $('span#totalPrice').html(total_price);
            $('input[name=totalPrice]').val(total_price);
        })
    })
</script>
<script src="/Public/Home/js/common.js"></script>
</body>
</html>