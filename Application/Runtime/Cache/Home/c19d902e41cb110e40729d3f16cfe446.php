<?php if (!defined('THINK_PATH')) exit();?><html>
<head>
    <meta charset="utf-8">
<title>智邮宝</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="blank" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="full-screen" content="yes">
<meta name="x5-fullscreen" content="true">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.mobile.css">
<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.css">
<script src="/Public/Common/plugin/layui-v2.2.6/layui/layui.js"></script>

<script src='http://app.lxh.magcloud.cc/public/static/dest/js/libs/magjs-x.js'></script>

<link rel="stylesheet" href="/Public/Common/css/common.css">
<link rel="stylesheet" href="/Public/Home/css/common.css">
</head>
<body>
<div class="layui-layout layui-layout-admin">
    <!--<h5>智游宝</h5>-->
    <div class="layui-header">
        <ul class="layui-nav layui-bg-blue">
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Index'): ?>layui-this<?php endif; ?>"><a href="/">首页</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Order'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Order/index');?>">订票</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'OrderReturn'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('OrderReturn/index');?>">退票申请</a></li>
        </ul>
    </div>
    <!--<h5>票付通</h5>-->
    <!--<div class="layui-header">-->
        <!--<ul class="layui-nav layui-bg-blue">-->
            <!--<li class="layui-nav-item layui-this"><a href="<?php echo U('Piaofutong/index');?>">首页</a></li>-->
        <!--</ul>-->
    <!--</div>-->
</div>
<div class="layui-carousel" id="test10">
    <div carousel-item="">
        <div><img src="<?php echo thumb($info['thumb']);?>"></div>
    </div>
</div>
<div class="seller">
    商家：<?php echo getFieldByModel('name','Seller',$info['link_seller'],'没有商家');?>
</div>
<div class="title">
    <?php echo ($info["name"]); ?>
</div>
<div class="sn">
    <?php echo ($info["sn"]); ?>
</div>
<div class="real">
    实名制：<?php echo getStatus($info['is_real']);?>
</div>
<div class="price">
    单价：<?php echo ($info["price"]); ?>
</div>
<div class="stock">
    库存：<?php echo ($info["stock"]); ?>
</div>
<div class="stock">
    状态：<?php echo getStatus($info['stock']);?>
</div>
<div class="content">
    商品详情：
    <div>
        <?php echo htmlspecialchars_decode($info['content']);?>
    </div>
</div>
<a href="<?php echo U('pay',array('id'=>I('get.id')));?>" class="layui-btn layui-btn-danger layui-btn-block">购买</a>
<script src="/Public/Home/js/common.js"></script>
<script>
    layui.use(['carousel', 'form'], function () {
        var carousel = layui.carousel
            , form = layui.form;
        //图片轮播
        carousel.render({
            elem: '#test10'
            , width: '100%'
            , interval: 5000
        });
    })
</script>
</body>
</html>