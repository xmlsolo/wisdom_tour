<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
<title>智邮宝</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="blank" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="full-screen" content="yes">
<meta name="x5-fullscreen" content="true">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.mobile.css">
<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.css">
<script src="/Public/Common/plugin/layui-v2.2.6/layui/layui.js"></script>

<script src='http://app.lxh.magcloud.cc/public/static/dest/js/libs/magjs-x.js'></script>

<!-- import Vue before Element -->
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<link rel="stylesheet" href="/Public/Common/css/common.css">
<link rel="stylesheet" href="/Public/Home/css/common.css">
</head>
<body>
<ul class="layui-nav layui-bg-blue header">
    <li class="layui-nav-item">
        <a href=""><i class="layui-icon">&#xe603;</i></a>
    </li>
    <li class="layui-nav-item title">
        <a href="javascript:;">东营</a>
        <dl class="layui-nav-child">
            <dd><a href="">济南</a></dd>
            <dd><a href="">青岛</a></dd>
            <dd><a href="">烟台</a></dd>
            <dd><a href="">潍坊</a></dd>
            <dd><a href="">威海</a></dd>
        </dl>
    </li>
    <li class="layui-nav-item fr">
        <a href=""><i class="layui-icon">&#xe715;</i></a>
    </li>
</ul>
<script>
    layui.use('element', function () {
        var element = layui.element;

    });
</script>
<table class="layui-table">
    <tr>
        <td>票名</td>
        <td>金额</td>
    </tr>
    <?php $start_date = '2018-05-08'; $end_date = '2018-05-08'; ?>
    <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
            <td>
                <a href="<?php echo U('pay',array('uuid'=>$vo['lid'] ,'ticket_id'=>$vo['uuid']));?>"><?php echo ($vo["title"]); ?></a>
            </td>
            <td><?php echo ($vo["tprice"]); ?></td>
        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
</table>
</body>
</html>