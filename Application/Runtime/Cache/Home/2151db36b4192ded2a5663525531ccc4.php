<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
<title>智邮宝</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="blank" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="full-screen" content="yes">
<meta name="x5-fullscreen" content="true">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.mobile.css">
<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.css">
<script src="/Public/Common/plugin/layui-v2.2.6/layui/layui.js"></script>

<script src='http://app.lxh.magcloud.cc/public/static/dest/js/libs/magjs-x.js'></script>

<link rel="stylesheet" href="/Public/Common/css/common.css">
<link rel="stylesheet" href="/Public/Home/css/common.css">
    <style>
        img{
            width: 100%;
        }
    </style>
</head>
<body>
<div class="layui-layout layui-layout-admin">
    <!--<h5>智游宝</h5>-->
    <div class="layui-header">
        <ul class="layui-nav layui-bg-blue">
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Index'): ?>layui-this<?php endif; ?>"><a href="/">首页</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Order'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Order/index');?>">订票</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'OrderReturn'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('OrderReturn/index');?>">退票申请</a></li>
        </ul>
    </div>
    <!--<h5>票付通</h5>-->
    <!--<div class="layui-header">-->
        <!--<ul class="layui-nav layui-bg-blue">-->
            <!--<li class="layui-nav-item layui-this"><a href="<?php echo U('Piaofutong/index');?>">首页</a></li>-->
        <!--</ul>-->
    <!--</div>-->
</div>
<p>标题:<?php echo ($info["title"]); ?></p>
<p>地址:<?php echo ($info["address"]); ?></p>
<p>区域:<?php echo ($info["area"]); ?></p>
<p>缩略图: <img src="<?php echo ($info["thumb"]); ?>" height="100%"></p>
<p>产品类型:<?php echo Piaofutong::get_p_type($info['p_type']);?></p>
<p>介绍:<?php echo ($info["bhjq"]); ?></p>
<p>级别:<?php echo ($info["j_type"]); ?></p>
<p>纬度(百度):<?php echo ($info["lng_lat_pos"]); ?></p>
<p>商家编号:<?php echo ($info["salerid"]); ?></p>
<p>联系电话:<?php echo ($info["tel"]); ?></p>
<p>联系传真:<?php echo ($info["fax"]); ?></p>
<p>相关提示:<?php echo ($info["jqts"]); ?></p>
<p>营业时间:<?php echo ($info["runtime"]); ?></p>
<p>开业时间:<?php echo ($info["opentime"]); ?></p>
<p>景区在售状态:<?php echo Piaofutong::get_status($info['status']);?></p>
<p>旅游主题:<?php echo ($info["topics"]); ?></p>
<p>交通指南:<?php echo ($info["jtzn"]); ?></p>
<a href="<?php echo U('getTicketlist',array('uuid'=>I('get.uuid')));?>" class="layui-btn layui-btn-danger layui-btn-block">购买</a>
</body>
</html>