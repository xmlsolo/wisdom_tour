<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
<title>我还没想好</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="apple-mobile-web-app-status-bar-style" content="blank"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="full-screen" content="yes">
<meta name="x5-fullscreen" content="true">

    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.mobile.css">
<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.css">
<script src="/Public/Common/plugin/layui-v2.2.6/layui/layui.js"></script>

<!-- import Vue before Element -->
<!--<script src="https://unpkg.com/vue/dist/vue.js"></script>-->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<link rel="stylesheet" href="/Public/Common/css/common.css">
<link rel="stylesheet" href="/Public/Home/css/common.css">
<script src="/Public/Home/js/common.js"></script>

</head>
<body>
<link rel="stylesheet" href="/Public/Home/css/list.css">

<div id="mask"></div>
<div class="list seller_list body_container" style="min-height: 315px">
    <ul id="list">
        <li v-for="item in items">
            <a :href="item.url">
                <div class="layui-row">
                    <div class="layui-col-xs4 thumb">
                        <img :src="item.thumb_img">
                    </div>
                    <div class="layui-col-xs8 info">
                        <div class="title">
                            <span class="title_text" v-cloak>{{item.name}}</span>
                        </div>
                        <div class="tips">
                            <span class="tip">随买随用</span>
                            <span class="tip">名胜古迹</span>
                        </div>
                        <div class="achievement">
                            <span class="score">4.6分</span>
                            <span class="recommend">97%推荐</span>
                            <span class="sell">已售2703份</span>
                        </div>
                        <div class="distance">
                            距您45.2公里
                        </div>
                    </div>
                </div>
            </a>
        </li>
    </ul>
</div>
<link rel="stylesheet" href="/Public/Home/css/footer.css">
<script src="/Public/Home/js/footer.js"></script>

<ul class="layui-nav footer">
    <li class="layui-nav-item">
        <a href="/index.php">
            <i class="layui-icon">&#xe68e;</i>
        </a>
    </li>
    <li class="layui-nav-item">
        <a href="javascript:;">
            <i class="layui-icon">&#xe669;</i>
        </a>
    </li>
    <li class="layui-nav-item">
        <a href="javascript:;">
            <i class="layui-icon">&#xe669;</i>
        </a>
    </li>
    <li class="layui-nav-item">
        <a href="javascript:;">
            <i class="layui-icon">&#xe770;</i>
        </a>
        <dl class="layui-nav-child layui-anim layui-anim-upbit">
            <dd><a href="<?php echo U('Order/index');?>">我的订单</a></dd>
            <dd><a href="<?php echo U('index',array('act'=>'ok'));?>">已核销</a></dd>
            <dd><a href="<?php echo U('index',array('act'=>'return'));?>">已退票</a></dd>
        </dl>
    </li>
</ul>

<script src="/Public/Home/js/index.js"></script>
</body>
</html>