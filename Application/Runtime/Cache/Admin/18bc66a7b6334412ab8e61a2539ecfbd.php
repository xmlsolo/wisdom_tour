<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
<title>胜利社区 - 商务管理系统</title>
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.css">
<script src="/Public/Common/plugin/layui-v2.2.6/layui/layui.js"></script>

<script src='http://app.lxh.magcloud.cc/public/static/dest/js/libs/magjs-x.js'></script>

<link rel="stylesheet" href="/Public/Common/css/common.css">
<link rel="stylesheet" href="/Public/Admin/css/common.css">
    <style>
        .layui-table tr td {
            font-size: 12px;
        }
    </style>
</head>
<body>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <ul class="layui-nav">
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Index'): ?>layui-this<?php endif; ?>"><a href="/index.php/Admin">主页</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Order'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Order/index');?>">订单</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Seller'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Seller/index');?>">景区</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Goods'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Goods/index');?>">票型</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'OrderReturn'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('OrderReturn/index');?>">退单申请</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Errorlog'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Errorlog/index');?>">错误</a></li>
            <li class="layui-nav-item"><a href="<?php echo U('Admin/index/logout');?>">退出</a></li>
        </ul>
    </div>
</div>
<form action="" method="get" class="layui-form">
    <div class="layui-row">
        <div class="layui-col-xs3">
            <input name="order_code" placeholder="单号" class="layui-input" value="<?php echo I('get.order_code');?>"/>
        </div>
        <div class="layui-col-xs3">
            <select name="link_seller">
                <option value="0">景区</option>
                <?php if(is_array($seller_list)): $i = 0; $__LIST__ = $seller_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"
                    <?php if(I('get.link_seller') == $vo['id']): ?>selected<?php endif; ?>
                    ><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
        </div>
        <div class="layui-col-xs3">
            <select name="act">
                <option value="0"
                <?php if(I('get.act') == 0): ?>selected<?php endif; ?>
                >订单状态</option>
                <option value="1"
                <?php if(I('get.act') == 1): ?>selected<?php endif; ?>
                >成功</option>
                <option value="2"
                <?php if(I('get.act') == 2): ?>selected<?php endif; ?>
                >已支付</option>
                <option value="3"
                <?php if(I('get.act') == 3): ?>selected<?php endif; ?>
                >待支付</option>
                <option value="4"
                <?php if(I('get.act') == 4): ?>selected<?php endif; ?>
                >退票</option>
            </select>
        </div>
        <div class="layui-col-xs3">
            <input type="submit" class="layui-btn layui-btn-normal" value="筛选">
        </div>
    </div>
</form>
<table class="layui-table">
    <tr>
        <!--<td>统一订单号</td>-->
        <td width="100">景区</td>
        <td width="120">单号</td>
        <!--<td width="120">远程订单号</td>-->
        <td width="112">添加时间</td>
        <td width="112">支付时间</td>
        <td width="48">姓名</td>
        <td width="60">电话</td>
        <td class="text-center" width="24">票数</td>
        <td class="text-center" width="24">验证</td>
        <td class="text-center" width="24">退数</td>
        <td style="text-align: right" width="40">总额</td>
        <td class="text-center" width="24">支付</td>
        <td class="text-center" width="24">接口</td>
        <td class="text-center" width="24">退票</td>
        <td class="text-center" width="24">状态</td>
        <td class="text-center">操作</td>
    </tr>
    <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
            <td>
                <div style="height: 20px;overflow: hidden">
                    <?php echo ($vo["s_name"]); ?>
                </div>
            </td>
            <td><?php echo ($vo["order_code"]); ?></td>
            <!--<td><?php echo ($vo["union_order_num"]); ?></td>-->
            <td><?php echo date('Y-m-d H:i:s',$vo['addtime']);?></td>
            <td>
                <?php if($vo['paytime']): echo date('Y-m-d H:i:s',$vo['paytime']);?>
                    <?php else: ?>
                    未支付<?php endif; ?>
            </td>
            <td><?php echo ($vo["real_name"]); ?></td>
            <td><?php echo ($vo["mobile"]); ?></td>
            <td class="text-center" style="color: blue;"><?php echo ($vo["num"]); ?></td>
            <td class="text-center">
                <?php if($vo['check_num']): ?><div style="color: red;"><?php echo ($vo["check_num"]); ?></div>
                    <?php else: ?>
                    <i style="color: green" class="layui-icon center">&#x1007;</i><?php endif; ?>
            </td>
            <td class="text-center">
                <?php if($vo['return_num']): ?><div style="color: red"><?php echo ($vo["return_num"]); ?></div>
                    <?php else: ?>
                    <i style="color: green" class="layui-icon center">&#x1007;</i><?php endif; ?>
            </td>
            <td style="text-align: right;color: red;font-weight: bold"><?php echo ($vo["total_price"]); ?></td>
            <td class="text-center"><?php echo getStatus($vo['is_pay']);?></td>
            <td class="text-center"><?php echo getStatus($vo['is_api']);?></td>
            <td class="text-center"><?php echo getStatus($vo['is_return']);?></td>
            <td class="text-center">
                <?php switch($vo['status']): case "0": ?><i style="color: red" class="layui-icon center">&#x1007;</i><?php break;?>
                    <?php case "1": ?><i style="color: green" class="layui-icon center">&#x1005;</i><?php break;?>
                    <?php case "2": ?><i style="color: blue" class="layui-icon center">&#xe63f;</i><?php break;?>
                    <?php default: ?>
                    <i style="color: red" class="layui-icon center">&#x1007;</i><?php endswitch;?>
            </td>
            <td>
                <a href="<?php echo U('detial',array('id'=>$vo['id']));?>" class="layui-btn layui-btn-xs">详细</a>
                <button data-pay="<?php echo ($vo["is_pay"]); ?>"
                        data-sign="<?php echo ($vo["sign"]); ?>"
                        data-order_code="<?php echo ($vo["order_code"]); ?>"
                        data-url="<?php echo U('verity_order' , array('uniton_order_num' => $vo['union_order_num']));?>"
                        class="layui-btn layui-btn-normal layui-btn-xs verity_pay">验证支付
                </button>
            </td>
        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
</table>
<div class="page">
    <?php echo ($page); ?>
</div>
<script>
    layui.use(['form', 'layer'], function () {
        var form = layui.form,
            layer = layui.layer;

        $('.verity_pay').click(function () {
            let self = $(this);
            $.ajax({
                url: $(this).attr('data-url'),
                data: {},
                beforeSend: function () {
                    layer.msg('验证中 ...');
                },
                success: function (req) {
                    let msg = req.paymsg;
                    if (req.paycode === 1 && self.attr('data-pay') === '0') {
                        layer.open({
                            content: '<div style="text-align: center">' + msg + '</div>' + '<br/>数据校准不正确!是否向 Api 发送申请!',
                            btn: ['发送', '取消'],
                            yes: function () {
                                let url = '/index.php/Home/Payment/piaofutong_unionOrderNumCallback/order_code/' + self.attr('data-order_code') + '/sign/' + self.attr('data-sign');
                                $.ajax({
                                    url: url,
                                    data: {},
                                    success: function (req) {
                                        window.location.reload()
                                    },
                                    beforeSend: function () {
                                        layer.msg('正在发送...')
                                    }
                                })
                            }
                        });
                    } else {
                        layer.msg(msg);
                    }
                }
            })
        })
    })
</script>
</body>
</html>