<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
<title>胜利社区 - 商务管理系统</title>
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.css">
<script src="/Public/Common/plugin/layui-v2.2.6/layui/layui.js"></script>

<script src='http://app.lxh.magcloud.cc/public/static/dest/js/libs/magjs-x.js'></script>

<link rel="stylesheet" href="/Public/Common/css/common.css">
<link rel="stylesheet" href="/Public/Admin/css/common.css">
</head>
<body>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <ul class="layui-nav">
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Index'): ?>layui-this<?php endif; ?>"><a href="/index.php/Admin">主页</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Order'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Order/index');?>">订单</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Seller'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Seller/index');?>">景区</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Goods'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Goods/index');?>">票型</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'OrderReturn'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('OrderReturn/index');?>">退单申请</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Errorlog'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Errorlog/index');?>">错误</a></li>
            <li class="layui-nav-item"><a href="<?php echo U('Admin/index/logout');?>">退出</a></li>
        </ul>
    </div>
</div>

</body>
</html>