<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
<title>胜利社区 - 商务管理系统</title>
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.css">
<script src="/Public/Common/plugin/layui-v2.2.6/layui/layui.js"></script>

<script src='http://app.lxh.magcloud.cc/public/static/dest/js/libs/magjs-x.js'></script>

<link rel="stylesheet" href="/Public/Common/css/common.css">
<link rel="stylesheet" href="/Public/Admin/css/common.css">
</head>
<body>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <ul class="layui-nav">
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Index'): ?>layui-this<?php endif; ?>"><a href="/index.php/Admin">主页</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Order'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Order/index');?>">订单</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Seller'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Seller/index');?>">景区</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Goods'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Goods/index');?>">票型</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'OrderReturn'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('OrderReturn/index');?>">退单申请</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Errorlog'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Errorlog/index');?>">错误</a></li>
            <li class="layui-nav-item"><a href="<?php echo U('Admin/index/logout');?>">退出</a></li>
        </ul>
    </div>
</div>
<table class="layui-table">
    <tr>
        <td width="100">订单号</td>
        <td><?php echo ($info["order_code"]); ?></td>
    </tr>
    <tr>
        <td>API</td>
        <td><?php echo ($info["goods_api"]); ?></td>
    </tr>
    <tr>
        <td>远端订单号</td>
        <td><?php echo ($info["union_order_num"]); ?></td>
    </tr>
    <tr>
        <td>签名</td>
        <td><?php echo ($info["sign"]); ?></td>
    </tr>
    <tr>
        <td>订票人</td>
        <td><?php echo ($info["real_name"]); ?></td>
    </tr>
    <tr>
        <td>账号ID</td>
        <td><?php echo ($info["user_id"]); ?></td>
    </tr>
    <tr>
        <td>联系方式</td>
        <td><?php echo ($info["mobile"]); ?></td>
    </tr>
    <tr>
        <td>订票时间</td>
        <td><?php echo date('Y-m-d H:i:s' ,$info['addtime']);?></td>
    </tr>
    <tr>
        <td>游玩时间</td>
        <td><?php echo date('Y-m-d H:i:s' ,$info['playtime']);?></td>
    </tr>
    <tr>
        <td>付款时间</td>
        <td><?php echo date('Y-m-d H:i:s' ,$info['paytime']);?></td>
    </tr>
</table>


<table class="layui-table">
    <tr>
        <td width="50">二维码</td>
        <td width="120">订单号</td>
        <td width="170">子订单号</td>
        <td>票名</td>
        <td width="130">核销时间</td>
        <td width="30">验证</td>
        <td width="30">退票</td>
        <td width="30">数量</td>
        <td width="30" class="text-center">退票</td>
        <td width="30" class="text-center">支付</td>
        <td width="30" class="text-center">状态</td>
    </tr>
    <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
            <td><a href="<?php echo ($vo["qr"]); ?>">二维码</a></td>
            <td><?php echo ($vo["order_code"]); ?></td>
            <td><?php echo ($vo["sub_order_code"]); ?></td>
            <td><?php echo ($vo["goods_name"]); ?></td>
            <td>
                <?php if($vo['dotime']): echo date('Y-m-d H:i:s',$vo['dotime']);?>
                    <?php else: ?>
                    未核销<?php endif; ?>
            </td>
            <td class="text-center"><?php echo ($vo["check_num"]); ?></td>
            <td class="text-center"><?php echo ($vo["return_num"]); ?></td>
            <td class="text-center"><?php echo ($vo["num"]); ?></td>
            <td class="text-center"><?php echo getStatus($vo['is_return']);?></td>
            <td class="text-center"><?php echo getStatus($vo['is_pay']);?></td>
            <td class="text-center"><?php echo getStatus($vo['is_status']);?></td>
        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
</table>

</body>
</html>