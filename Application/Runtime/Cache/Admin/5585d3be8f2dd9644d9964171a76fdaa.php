<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
<title>胜利社区 - 商务管理系统</title>
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.css">
<script src="/Public/Common/plugin/layui-v2.2.6/layui/layui.js"></script>

<script src='http://app.lxh.magcloud.cc/public/static/dest/js/libs/magjs-x.js'></script>

<link rel="stylesheet" href="/Public/Common/css/common.css">
<link rel="stylesheet" href="/Public/Admin/css/common.css">
</head>
<body>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <ul class="layui-nav">
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Index'): ?>layui-this<?php endif; ?>"><a href="/index.php/Admin">主页</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Order'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Order/index');?>">订单</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Seller'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Seller/index');?>">景区</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Goods'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Goods/index');?>">票型</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'OrderReturn'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('OrderReturn/index');?>">退单申请</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'ErrorLog'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Errorlog/index');?>">错误</a></li>
            <li class="layui-nav-item"><a href="<?php echo U('Admin/index/logout');?>">退出</a></li>
        </ul>
    </div>
</div>
<form action="" method="post" class="layui-form">
    <table class="layui-table">
        <tr>
            <td width="60">关联商家</td>
            <td>
                <select name="link_seller" lay-search="">
                    <option value="">直接选择或搜索选择</option>
                    <?php if(is_array($list_seller)): $i = 0; $__LIST__ = $list_seller;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>" <?php if($vo['id'] == $info['link_seller']): ?>selected<?php endif; ?> ><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                </select>

            </td>
        </tr>
        <tr>
            <td>商品封面</td>
            <td>
                <button type="button" class="layui-btn layui-btn-danger" id="test1">上传图片</button>
                <input type="hidden" name="thumb" value="<?php echo ($info["thumb"]); ?>">
                <div class="layui-upload-list">
                    <img class="layui-upload-img" src="/Uploads/<?php echo ($info["thumb"]); ?>" id="demo1">
                    <p id="demoText"></p>
                </div>
            </td>
        </tr>
        <tr>
            <td>票型</td>
            <td><input type="text" name="sn" class="layui-input" value="<?php echo ($info["sn"]); ?>"></td>
        </tr>
        <tr>
            <td>商品标题</td>
            <td><input type="text" name="name" class="layui-input" value="<?php echo ($info["name"]); ?>"></td>
        </tr>
        <tr>
            <td>商品单价</td>
            <td><input type="text" name="price" class="layui-input" value="<?php echo ($info["price"]); ?>"></td>
        </tr>
        <tr>
            <td>商品库存</td>
            <td><input type="text" name="stock" class="layui-input" value="<?php echo ($info["stock"]); ?>"></td>
        </tr>
        <tr>
            <td>销售时间</td>
            <td>
                <div class="layui-inline">
                    <input type="text" name="starttime" class="layui-input test-item" value="<?php echo date('Y-m-d',$info['starttime']);?>" placeholder="yyyy-MM-dd">
                </div>
                <div class="layui-inline">
                    <input type="text" name="overtime" class="layui-input test-item" value="<?php echo date('Y-m-d',$info['overtime']);?>" placeholder="yyyy-MM-dd">
                </div>
            </td>
        </tr>
        <tr>
            <td>商品详情</td>
            <td>
                <script id="container" name="content" name="content" type="text/plain"><?php echo htmlspecialchars_decode($info['content']);?></script>
            </td>
        </tr>
        <tr>
            <td>游玩时间</td>
            <td>
                <input type="checkbox" <?php if($info['is_play_date']): ?>checked<?php endif; ?> name="is_play_date" lay-skin="switch" lay-filter="switchTest"
                       lay-text="有效|无效">
            </td>
        </tr>
        <tr>
            <td>实名制</td>
            <td>
                <input type="checkbox" <?php if($info['is_real']): ?>checked<?php endif; ?> name="is_real" lay-skin="switch" lay-filter="switchTest"
                       lay-text="开启|关闭">
            </td>
        </tr>
        <tr>
            <td>商品状态</td>
            <td>
                <input type="checkbox" <?php if($info['status']): ?>checked<?php endif; ?> name="status" lay-skin="switch" lay-filter="switchTest"
                       lay-text="上架|下架">
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" class="layui-btn layui-btn-normal layui-btn-block"></td>
        </tr>
    </table>
</form>
<!-- 配置文件 -->
<script type="text/javascript" src="/Public/Admin/plugin/ueditor1_4_3_3-utf8-php/utf8-php/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/Public/Admin/plugin/ueditor1_4_3_3-utf8-php/utf8-php/ueditor.all.js"></script>
<!-- 实例化编辑器 -->
<script type="text/javascript">
    var ue = UE.getEditor('container');
</script>
<script>
    layui.use(['form', 'laydate', 'upload'], function () {
        var $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            upload = layui.upload;
        laydate.render({
            elem: 'input[name=starttime]'
        });
        laydate.render({
            elem: 'input[name=overtime]'
        });


        //普通图片上传
        var uploadInst = upload.render({
            elem: '#test1'
            , url: "<?php echo U('update_pic');?>"
            , before: function (obj) {
                //预读本地文件示例，不支持ie8
                obj.preview(function (index, file, result) {
                    $('#demo1').attr('src', result); //图片链接（base64）
                });
            }
            , done: function (res) {
                //如果上传失败
                console.log(res);
                if (!res.status) {
                    return layer.msg('上传失败');
                }
                else {
                    $('input[name=thumb]').val(res.info.file.savepath + res.info.file.savename);
                    return layer.msg('上传成功');
                }
            }
            , error: function () {
                //演示失败状态，并实现重传
                var demoText = $('#demoText');
                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function () {
                    uploadInst.upload();
                });
            }
        });
    })
</script>
</body>
</html>