<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
<title>胜利社区 - 商务管理系统</title>
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.css">
<script src="/Public/Common/plugin/layui-v2.2.6/layui/layui.js"></script>

<script src='http://app.lxh.magcloud.cc/public/static/dest/js/libs/magjs-x.js'></script>

<link rel="stylesheet" href="/Public/Common/css/common.css">
<link rel="stylesheet" href="/Public/Admin/css/common.css">
    <link rel="stylesheet" href="/Public/Admin/css/login.css">
</head>
<body>
<div>
    <div id="title">
        <div id="logo">
            <img src="/Public/Common/image/logo_alpha_512.png" width="75" height="75">
        </div>
        <div class="text">
            胜利社区 商务系统
        </div>
    </div>
    <div class="login">
        <div class="news fl">
            <div class="layui-carousel">
                <div carousel-item="">
                    <div>
                        <img src="/Public/Admin/image/new_1.jpg" width="425" height="305">
                    </div>
                    <div>
                        <img src="/Public/Admin/image/new_1.jpg" width="425" height="305">
                    </div>
                </div>
            </div>
        </div>
        <form action="<?php echo U('login');?>" id="login" class="layui-form fr" method="post">
            <div class="layui-form-item">
                <input type="text" name="username" id="username" autocomplete="off" placeholder="用户名"
                       class="layui-input" lay-verify="username" onautocomplete="false">
            </div>
            <div class="layui-form-item">
                <input type="password" name="password" id="password" autocomplete="off" placeholder="密码"
                       class="layui-input" lay-verify="password" onautocomplete="false">
            </div>
            <div class="layui-form-item">
                <div class="layui-row">
                    <div class="layui-col-xs7">
                        <input type="text" name="vcode" id="vcode" autocomplete="off" placeholder="验证码"
                               class="layui-input" lay-verify="vcode" onautocomplete="false" maxlength="4">
                    </div>
                    <div class="layui-col-xs5" style="text-align: right;padding-right: 7px;">
                        <img src="<?php echo U('getVcode');?>" height="36" id="vcode_img">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <input type="checkbox" lay-skin="primary" title="记住 账号">
            </div>
            <div class="layui-form-item">
                <button class="layui-btn layui-btn-fluid layui-btn-normal" lay-filter="login" lay-submit="">登录</button>
            </div>
        </form>
    </div>
</div>
<script>
    layui.use(['carousel', 'form'], function () {
        var carousel = layui.carousel
            , form = layui.form;

        // 验证
        form.verify({
            username: function (value) {
                if (value.length < 1) {
                    return '必须有用户名';
                }
            },
            password: function (value) {
                if (value.length < 1) {
                    return '必须有密码';
                }
            },
            vcode: function (value) {
                if (value.length < 1) {
                    return '必须有验证码';
                }
            }
        });

        carousel.render({
            elem: '.layui-carousel',
            interval: 3000,
            anim: 'fade',
            width: '425',
            height: '305'
        });
    });
    $(function () {
        $('#username').focus();
        $('#vcode_img').click(function () {
            $(this).attr('src' , '<?php echo U("getVcode");?>');
        })
    })
</script>
<style>
    .footer{
        position: absolute;
        left: 0;
        bottom: 0;
        width: 100%;
        line-height: 30px;
        padding: 20px;
        text-align: center;
        box-sizing: border-box;
        color: rgba(0,0,0,.5);
    }
</style>
<div class="footer">
    <p>© 2018 胜利社区(东营)</p>
</div>
</body>
</html>