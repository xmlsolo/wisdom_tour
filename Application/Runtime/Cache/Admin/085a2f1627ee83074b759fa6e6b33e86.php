<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
<title>胜利社区 - 商务管理系统</title>
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="/Public/Common/plugin/layui-v2.2.6/layui/css/layui.css">
<script src="/Public/Common/plugin/layui-v2.2.6/layui/layui.js"></script>

<script src='http://app.lxh.magcloud.cc/public/static/dest/js/libs/magjs-x.js'></script>

<link rel="stylesheet" href="/Public/Common/css/common.css">
<link rel="stylesheet" href="/Public/Admin/css/common.css">
    <style>
        .layui-table tr td {
            font-size: 12px;
        }
    </style>
</head>
<body>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <ul class="layui-nav">
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Index'): ?>layui-this<?php endif; ?>"><a href="/index.php/Admin">主页</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Order'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Order/index');?>">订单</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Seller'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Seller/index');?>">景区</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Goods'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Goods/index');?>">票型</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'OrderReturn'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('OrderReturn/index');?>">退单申请</a></li>
            <li class="layui-nav-item <?php if(CONTROLLER_NAME == 'Errorlog'): ?>layui-this<?php endif; ?>"><a href="<?php echo U('Errorlog/index');?>">错误</a></li>
            <li class="layui-nav-item"><a href="<?php echo U('Admin/index/logout');?>">退出</a></li>
        </ul>
    </div>
</div>
<table class="layui-table">
    <tr>
        <td>ID</td>
        <td>退单号</td>
        <td>订单号</td>
        <td>用户ID</td>
        <td>订单ID</td>
        <td>商品模型</td>
        <td>数量</td>
        <td>信息</td>
        <td>提交时间</td>
        <td>处理时间</td>
        <td>处理状态</td>
        <td>通过/驳回</td>
        <td>操作</td>
    </tr>
    <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
            <td><?php echo ($vo["id"]); ?></td>
            <td><?php echo ($vo["return_order_code"]); ?></td>
            <td><?php echo ($vo["order_code"]); ?></td>
            <td><?php echo ($vo["user_id"]); ?></td>
            <td><?php echo ($vo["order_id"]); ?></td>
            <td><?php echo ($vo["goods_model"]); ?></td>
            <td><?php echo ($vo["num"]); ?></td>
            <td><?php echo ($vo["message"]); ?></td>
            <td><?php echo date('Y-m-d H:i:s',$vo['addtime']);?></td>
            <td>
                <?php if($vo['overtime']): ?><font color="red"><?php echo date('Y-m-d H:i:s',$vo['overtime']);?></font>
                    <?php else: ?>
                    <font color="blue">未处理</font><?php endif; ?>
            </td>
            <td><?php echo getStatus($vo['status']);?></td>
            <td>
                <?php switch($vo['is_success']): case "0": ?><font color="blue">待处理</font><?php break;?>
                    <?php case "1": ?><font color="green">通过</font><?php break;?>
                    <?php case "-1": ?><font color="red">驳回</font><?php break; endswitch;?>
            <td>
                <?php if($vo['status'] == 1 && $vo['is_success'] == -1): ?><span class="back_message layui-btn layui-btn-normal layui-btn-xs"
                          data-message="<?php echo ($vo["return_message"]); ?>">原因</span>
                    <?php else: ?>
                    <a class="layui-btn layui-btn-normal layui-btn-xs"
                       href="<?php echo U('accept_piaofutong_return',array('id'=>$vo['id']));?>">接口受理</a>
                    <?php if($vo['status'] == 0): ?><a class="layui-btn layui-btn-xs"
                           href="<?php echo U('accept_piaofutong_return',array('id'=>$vo['id']));?>">通过</a>
                        <?php else: ?>
                        <a class="layui-btn layui-btn-xs" href="<?php echo U('act_accept',array('id'=>$vo['id']));?>">通过</a><?php endif; ?>
                    <a class="layui-btn layui-btn-danger layui-btn-xs"
                       href="<?php echo U('backOrderReturn',array('id'=>$vo['id']));?>">驳回</a><?php endif; ?>
                <button class="r_msg layui-btn layui-btn-warm layui-btn-xs" data-info="<?php echo ($vo['return_message']); ?>">信息
                </button>
            </td>
        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
</table>
<div class="page">
    <?php echo ($page); ?>
</div>
<script>
    layui.use('layer', function () {
        $('.back_message').click(function () {
            var self = $(this);
            layer.open({
                id: 'layerReturnMessage'
                , content: self.attr('data-message')
                , btnAlign: 'c'
                , shade: .3
            });
        });
        $('.r_msg').click(function () {
            var self = $(this);
            let content = '没有信息';
            if (self.attr('data-info'))
                content = self.attr('data-info');
            layer.open({
                id: 'layerReturnMessage'
                , content: content
                , btnAlign: 'c'
                , shade: .3
            });
        })
    })
</script>
</body>
</html>