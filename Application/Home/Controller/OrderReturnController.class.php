<?php

namespace Home\Controller;

use Common\Controller\MemberBaseController;
use Common\Model\OrderReturnModel;

class OrderReturnController extends MemberBaseController
{

    function index()
    {
        $model_order = new OrderReturnModel();
        $where = array('user_id' => session('user_id'));
        $pageSize = 12;
        $list = $this->displayPageList($model_order, $pageSize, $where, 'id desc');
        $this->assign('list', $list);
        $this->display();
    }
}