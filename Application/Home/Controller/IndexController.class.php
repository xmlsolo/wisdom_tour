<?php

namespace Home\Controller;

use Common\Controller\MemberBaseController;
use Common\Model\GoodsModel;
use Common\Model\SellerModel;
use Think\Controller;
use Vendor\Zhiyoubao\Zhiyoubao\Zhiyoubao;

class IndexController extends Controller
{
    private $zhiyoubao;
    private $piaofutong;

    function __construct()
    {
        parent::__construct();
        $website = "http://plugin.slit.cn/";
        // 智游宝
//        vendor("Zhiyoubao.Zhiyoubao");
//        $this->zhiyoubao = new Zhiyoubao($website);
        // 票付通
        vendor('Piaofutong.Piaofutong');
        $this->piaofutong = new \Piaofutong();
    }

    function index()
    {
        $model_goods = new SellerModel();
        $list = $model_goods->where(array('status' => 1))->order('id desc')->select();

        // 票付通
        $req = $this->piaofutong->scenicSpotList();
        if (!$req['success']) $this->error($req['info']);
        $list = array_merge($list, $req['info']);

        $this->assign('list', $list);
        $this->display(":index");
    }

    function ajax_get_seller_list()
    {
        $list = [];
//        $model_goods = new SellerModel();
//        $list = $model_goods->where(array('status' => 1))->order('id desc')->select();

        // 票付通
        $req = $this->piaofutong->scenicSpotList(0, 60);
        if (!$req['success'])
            $this->error(\Piaofutong::SOAP_ERROE);
        $list = array_merge($list, $req['info']);

        $req = array();
        foreach ($list as $vo) {
            $_arr = $vo;
            switch ($vo['model']) {
                case 'zhiyoubao':
                    $url = U('Zhiyoubao/scenicSpotInfo', array('id' => $vo['id']));
                    break;
                case 'piaofutong':
                    $url = U('Piaofutong/scenicSpotInfo', array('uuid' => $vo['uuid']));
                    break;
            };
            $_arr['url'] = $url;
            $_arr['thumb_img'] = thumb($vo['thumb']);
            $req['list'][] = $_arr;
        }
        $page_size = 4;
        $req['list'] = array_slice($req['list'], (I('get.p') - 1) * $page_size, $page_size);
        $req['pages'] = ceil(count($list) / $page_size);

        $this->success($req);
    }

}