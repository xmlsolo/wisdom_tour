<?php

// 随机字符串
function random_string($length = 8, $chars = '1234567890')
{
    $password = '';
    for ($i = 0; $i < $length; $i++) {
        $password .= $chars[mt_rand(0, strlen($chars) - 1)];
    }
    return $password;
}

/*
 * url_id_parm = array(0=>array('url'=>'xxx' ,'model'=>'id','id'=>'uuid'))
 */
function url($model_type, $url_id_parm = array())
{
    return U($url_id_parm[$model_type]['url'], array($url_id_parm[$model_type]['model'] => $url_id_parm[$model_type]['id']));
}

function thumb($thumb_url, $default_url = "/Public/Home/image/default.png")
{
    if (!$thumb_url)
        $url = $default_url;
    else {
        if (substr($thumb_url, 0, 7) == 'http://' or substr($thumb_url, 0, 8) == 'https://')
            $url = $thumb_url;
        else
            $url = '/Uploads/' . $thumb_url;
    }
    return $url;
}

function getStatus($status, $trueMsg = '<i style="color: green" class="layui-icon center">&#x1005;</i>',
                   $falseMsg = '<i style="color: red" class="layui-icon center">&#x1007;</i>')
{
    return $status ? $trueMsg : $falseMsg;
}

function getFieldByModel($field, $model, $where, $errMsg = false)
{
    if (!$errMsg)
        $errMsg = '找不到 Field ' . $field;
    $modelName = 'Common\\Model\\' . $model . 'Model';
    $model = new $modelName;
    $req = $model->where($where)->getField($field);
    return $req ? $req : $errMsg;
}

//    递归
function recursion($arr, $index = 0)
{
    $list = array();
    foreach ($arr as $v) {
        if ($v['pid'] == $index) {
            $v['children'] = recursion($arr, $v['id']);
            if (empty($v['children'])) {
                unset($v['children']);
            }
            array_push($list, $v);
        }
    }
    return $list;
}

/*
* httpGet请求
*/
function HttpGet($url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 500);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
    curl_setopt($curl, CURLOPT_URL, $url);
    $res = curl_exec($curl);
    curl_close($curl);
    $res = json_decode($res, true);
    return $res;
}

/*
* httpPost请求
*/
function HttpPost($url, $data)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $return = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($return, true);
    return $return;
}

function str_insert($str, $i, $substr)
{
    $start = substr($str, 0, $i);
    $end = substr($str, $i);
    $str = ($start . $substr . $end);
    return $str;
}


function format_str($str)
{
    $str = str_replace('【', '<br/><br/>【', $str);
    $str = str_replace('】', '】<br/>', $str);
    $str = str_replace('2.', '<br/>2.', $str);
    $str = str_replace('3.', '<br/>3.', $str);
    $str = str_replace('4.', '<br/>4.', $str);
    $str = str_replace('5.', '<br/>5.', $str);
    $str = str_replace('6.', '<br/>6.', $str);
    $str = str_replace('7.', '<br/>7.', $str);
    $str = str_replace('8.', '<br/>8.', $str);
    $str = str_replace('9.', '<br/>9.', $str);
    $str = str_replace('②.', '<br/>②.', $str);
    $str = str_replace('③.', '<br/>③.', $str);
    $str = str_replace('④.', '<br/>④.', $str);
    $str = str_replace('⑤.', '<br/>⑤.', $str);
    $str = substr($str, 10, strlen($str));
    return $str;
}

function cut_str($str, $length = 8)
{
    if (mb_strlen($str) <= $length)
        return $str;
    return mb_substr($str, 0, $length, 'utf-8') . '...';
}

function has_return($sub_order_code)
{
    $where = array('order_code' => $sub_order_code);
    $model = new \Common\Model\OrderReturnModel();
    return $model->where($where)->find();
}
