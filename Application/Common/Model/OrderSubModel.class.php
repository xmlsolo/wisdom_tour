<?php

namespace Common\Model;

use Think\Model;

class OrderSubModel extends Model
{
    protected $_validate = array(
//        array('union_order_num', 'require', '统一订单号 必须！'),
        array('sub_order_code', 'require', '子订单号 必须！'),

        array('goods_id', 'require', '商品 ID 必须！'),
        array('goods_name', 'require', '商品 名称 必须！'),
        array('seller_id', 'require', '商家ID 必须！'),
        array('goods_api', 'require', '商品 名称 必须！'),
        array('addtime', 'require', '添加时间 必须！'),
        array('goods_sn', 'require', '商品 票型 必须！'),
        array('user_id', 'require', '用户 ID 必须！'),
//        array('real_name', 'require', '真实姓名 必须！'),
//        array('certificate_no', 'require', '身份证号 必须！'),
//        array('mobile', 'require', '联系电话 必须！'),
        array('order_code', 'require', '主订单号 必须！'),
        array('price', 'require', '单价 必须！'),
        array('num', 'require', '数量 必须！'),
        array('total_price', 'require', '总金额 必须！'),
        array('pay_type', 'require', '支付方式 必须！'),
        array('is_api', 'require', '通过api 必须！'),
        array('is_pay', 'require', '已支付 必须！'),
        array('is_status', 'require', '订单状态 必须！'),
        array('is_return', 'require', '退票 必须！'),

//        array('order_code','','主订单号 已经存在！',0,'unique',1),
        array('sub_order_code','','子订单号 已经存在！',0,'unique',1)
    );
}