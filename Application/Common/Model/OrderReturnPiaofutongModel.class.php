<?php

namespace Common\Model;

use Think\Model;

class OrderReturnPiaofutongModel extends Model
{
    protected $_validate = array(
        array('return_order_code', 'require', '退单号 必须！'),
        array('return_order_id', 'require', '退单ID 必须！'),
        array('order_id', 'require', '订单ID 必须！'),
        array('piaofutong_order_id', 'require', '票付通订单号 必须！'),
    );
}