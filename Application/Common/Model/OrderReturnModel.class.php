<?php

namespace Common\Model;

use Think\Model;

class OrderReturnModel extends Model
{
    protected $_validate = array(
        array('return_order_code', 'require', '退单 号 必须！'),
        array('goods_model', 'require', '商品模型 必须！'),
        array('user_id', 'require', '用户ID 必须！'),
        array('order_id', 'require', '订单ID 必须！'),
        array('addtime', 'require', '添加时间 必须！')
    );

    static function get_return_order_code()
    {
        return date('YmdHis', time()) . random_string(5);
    }
}