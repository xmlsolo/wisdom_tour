<?php

namespace Common\Model;

use Think\Model;

class OrderPiaofutongModel extends Model
{
    protected $_validate = array(
        array('order_id', 'require', '订单号 必须！'),
        array('ordernum', 'require', '票付通订单号 必须！'),
        array('remotenum', 'require', '远端订单号 必须！')
    );
}