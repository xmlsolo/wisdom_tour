<?php

namespace Common\Model;

use Think\Model;

class OrderModel extends Model
{
    protected $_validate = array(
        array('order_code', 'require', '主订单号 必须！'),
        array('addtime', 'require', '添加时间 必须！'),
        array('s_id', 'require', '景区ID 必须！'),
        array('s_name', 'require', '景区名称 必须！'),
        array('playtime', 'require', '游玩时间 必须！'),
        array('user_id', 'require', '用户ID 必须！'),
        array('certificate_no', 'require', '身份证号 必须！'),
        array('mobile', 'require', '联系方式 必须！'),
        array('total_price', 'require', '总额 必须！')
    );
}