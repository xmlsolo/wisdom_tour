<?php

namespace Common\Model;

use Think\Model;

class SellerModel extends Model
{
    protected $_validate = array(
        array('name', 'require', '商家名称 必须！'),
        array('name', '', '商家名称 已经存在！', 0, 'unique', 1)
    );
}