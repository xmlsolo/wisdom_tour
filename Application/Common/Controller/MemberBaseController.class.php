<?php

namespace Common\Controller;

use Think\Controller;

class MemberBaseController extends Controller
{

    function __construct()
    {
        parent::__construct();
        $app_url = C('MAG_APP_URL');
        $Secret = C('MAG_SECRET');
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $info = strstr($userAgent, "MAGAPPX");
        $info = explode("|", $info);
        $agent = ["version" => $info[1],//客户端版本信息 4.0.0
            "client" => $info[2],//客户端信息可以获取区分android或ios 可以获取具体机型
            "site" => $info[3],  //站点名称  如lxh
            "device" => $info[4],//设备号    客户端唯一设备号
            "sign" => $info[5],  //签名     可以验证请求是否伪造 签名算法见下面详细说明
            "signCloud" => $info[6],//云端签名 这个签名马甲内部使用
            "token" => $info[7]];   //用户token 用户的token信息
        $url = $app_url . "/mag/cloud/cloud/getUserInfo?token=" . $agent['token'] . "&secret=$Secret";
        $req = file_get_contents($url);
        $req_arr = json_decode($req, true);
        $status = $req_arr['success'];
        $code = $req_arr['code'];
        $data = $req_arr['data'];
        if (!$status or $code != 100) {
            $this->error('请您使用APP登录！');
        }
        session('user', $data);
        session('user_id', $data['user_id']);
    }

    protected function displayPageList($model, $pageSize = 12, $where = array(), $order = 'id desc')
    {
        $count = $model->where($where)->count();
        $Page = new \Think\Page($count, $pageSize);
        $nowPage = I('p');
        $show = $Page->show();
        $this->assign('page', $show);
        $option = '';
        $pageSelect = '';
        for ($i = 1; $i <= $Page->totalPages; $i++) {
            if (intval($nowPage) == $i)
                $selected = 'selected = "selected"';
            else
                $selected = '';
            $option .= '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';
        }
        if ($Page->totalPages > 1)
            $pageSelect .= '<form action="" method="get"><select name="p">' . $option . '</select><input type="submit" class="layui-btn layui-btn-normal layui-btn-xs" value="跳转"></form>';
        $this->assign('pageSelect', $pageSelect);
        $list = $model->where($where)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return $list;
    }

}