<?php

namespace Common\Controller;

use Common\Model\ErrorLogModel;
use Think\Controller;

class NoticeBaseController extends Controller
{

    function __construct()
    {
        parent::__construct();
    }


    //    复写 error
    protected function error($message = '', $is_log = false, $jumpUrl = '', $ajax = false)
    {
        if ($is_log) {
            $model_error_log = new ErrorLogModel();
            $data = array(
                'addtime' => time(),
                'controller' => CONTROLLER_NAME,
                'action' => ACTION_NAME,
                'msg' => $message
            );
            if (!$model_error_log->add($data))
                $this->errFile($message);
        }
        parent::error($message, $jumpUrl, $ajax);
    }

    protected function errFile($msg)
    {
        $myfile = fopen("verification.txt", "w");
        fwrite($myfile, $msg);
        fclose($myfile);
    }
}