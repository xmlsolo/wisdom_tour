<?php
// 快速 筛选样式
function filter($name, $type, $filed, $follow = '', $placeholder = '', $classType = 'layui-input')
{
    $html = '';
    switch ($type) {
        case 'input':
            $html = '<input type="text" name="' . $name . '" value="' . $follow . '" class="' . $classType . '" placeholder="' . $placeholder . '">';
            break;
        case 'select':
            if (!$filed)
                $option = '<option value="0">' . $placeholder . '</option>';
            else {
                $option = '';
                foreach ($filed as $key => $item) {
                    if (intval($follow) == $key)
                        $selected = 'selected="selected"';
                    else {
                        $selected = '';
                    }
                    $option .= '<option value="' . $key . '" ' . $selected . '>' . $item . '</option>';
                }
            }
            $html = '<select name="' . $name . '" class="' . $classType . '">' . $option . '</select>';
            break;
    }
    return $html;
}

// 快速 获得对应字段 return array($key_name=>$field_name)
function get_filed_list($model, $field_name, $key_name = 'id', $where = '', $errMsg = '')
{
    if (!$errMsg)
        $errMsg = '找不到 Field ' . $field_name;
    $modelName = 'Common\\Model\\' . $model . 'Model';
    $model = new $modelName;
    $field = $key_name . ',' . $field_name;
    $req = $model->where($where)->field($field)->select();
    $req_arr = array();
    foreach ($req as $item) {
        $req_arr[$item[$key_name]] = $item[$field_name];
    }
    return $req_arr ? $req_arr : $errMsg;
}

// 快速 format like 搜索where
function where_like($where_value, $type = 0)
{
    switch ($type) {
        case 0:
            $like = '%' . $where_value . '%';
            break;
        case 1:
            $like = '%' . $where_value;
            break;
        case 2:
            $like = $where_value . '%';
            break;
        default:
            $like = '%' . $where_value . '%';
            break;
    }
    $arr = array('like', $like);
    return $arr;
}

// 格式化 where
function format_where($where, $type = 'OR')
{
    if (count($where) > 1) {
        $where['_logic'] = $type;
    }
    return $where;
}