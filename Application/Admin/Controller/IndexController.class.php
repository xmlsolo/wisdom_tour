<?php

namespace Admin\Controller;


use Common\Controller\AdminBaseController;

class IndexController extends AdminBaseController
{
    function index()
    {
        $this->display(":index");
    }

    function logout()
    {
        parent::logout();
    }
}