<?php

namespace Admin\Controller;

use Common\Controller\AdminBaseController;
use Common\Model\OrderModel;
use Common\Model\OrderPiaofutongModel;
use Common\Model\OrderSubModel;
use Common\Model\SellerModel;
use Vendor\Zhiyoubao\Zhiyoubao\Zhiyoubao;

class OrderController extends AdminBaseController
{
    function index()
    {
        vendor('Zhiyoubao.Zhiyoubao');
        $model_order = new OrderModel();
        $model_seller = new SellerModel();

        $seller_list = $model_seller->select();
        $this->assign('seller_list', $seller_list);

        // 筛选
        $data = I('get.');
        $where = array();

        $order_code = $data['order_code'];
        if ($order_code)
            $where['order_code'] = array('like', '%' . $order_code . '%');

        $act = $data['act'];
        if ($act) {
            switch ($act) {
                case 1: // 成功
                    $where['is_status'] = 1;
                    break;
                case 2: // 已支付
                    $where['is_pay'] = 1;
                    break;
                case 3: // 待支付
                    $where['is_pay'] = 0;
                    break;
                case 4: // 退票
                    $where['is_return'] = 1;
                    break;
            }
        }

        $pageSize = 14;

        // 景区
        $link_seller = I('get.link_seller');

        if ($link_seller) {
            $where['zyb_goods.link_seller'] = $link_seller;
            $count = $model_order->join('RIGHT JOIN zyb_goods ON zyb_order.goods_id = zyb_goods.id')->order('zyb_order.id desc')->where($where)->count();
            $Page = new \Think\Page($count, $pageSize);
            $show = $Page->show();
            $field = 'zyb_order.id,zyb_order.order_code,zyb_order.goods_sn,zyb_order.goods_id,zyb_order.nick_name,zyb_order.phone,zyb_order.price,zyb_order.num,zyb_order.total_price,zyb_order.pay_type,zyb_order.is_pay,zyb_order.is_api,zyb_order.is_status';
            $list = $model_order->join('RIGHT JOIN zyb_goods ON zyb_order.goods_id = zyb_goods.id')->order('zyb_order.id desc')->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->field($field)->select();
            $this->assign('page', $show);
        } else {
            $list_arr = $this->displayPageList($model_order, $pageSize, $where, 'id desc');
            $list = $list_arr;
        }
        $this->assign('list', $list);
        $this->assign('pay_type', Zhiyoubao::PAY_TYPE_CN());
        $this->display();
    }

    function detial()
    {
        $id = $this->getId();
        $model_order = new OrderModel();
        $model_order_sub = new OrderSubModel();
        $model_order_piaofutong = new OrderPiaofutongModel();
        $info = $model_order->find($id);
        $list_order_sub = $model_order_sub->where(array('order_id' => $id))->select();
        $list = [];

        foreach ($list_order_sub as $order) {
            $_arr = $order;
            $info_piaofutong = $model_order_piaofutong->where(array('order_id' => $order['id']))->find();
            $_arr['qr'] = $info_piaofutong['qrcodeurl'];
            $list[] = $_arr;
        }
        $this->assign('info', $info);
        $this->assign('list', $list);
        $this->display();
    }

    function verity_order()
    {
        $data = I('get.');
        $union_order_num = $data['uniton_order_num'];
        $url = C('MAG_APP_URL') . '/core/pay/pay/orderStatusQuery?unionOrderNum=' . $union_order_num . '&secret=' . C('MAG_SECRET');
        $req = file_get_contents($url);
        $req_arr = json_decode($req, true);
//        $req_arr['paymsg'] = iconv('UTF-8', 'GBK', $req_arr['paymsg']);
        $this->ajaxReturn($req_arr);
    }
}