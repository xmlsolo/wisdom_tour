<?php

namespace Admin\Controller;

use Common\Controller\AdminBaseController;
use Common\Model\OrderModel;
use Common\Model\OrderPiaofutongModel;
use Common\Model\OrderReturnModel;
use Common\Model\OrderSubModel;
use Vendor\Zhiyoubao\Zhiyoubao\Zhiyoubao;

class OrderReturnController extends AdminBaseController
{
    function index()
    {
        $model_order_return = new OrderReturnModel();
        $list = $this->displayPageList($model_order_return);
        $this->assign('list', $list);
        $this->display();
    }

//    function act_return_ticket_notice()
//    {
//        $model_order_return = new OrderReturnModel();
//        $model_order = new OrderModel();
//        $data = I();
//
//        $myfile = fopen("notice.txt", "w") or die("Unable to open file!");
//        $txt = json_encode($data);
//        fclose($myfile);

    //改变退票状态
//        $save_is_success = array('is_success' => 1);
//        if (!$model_order_return->where(array('id' => $id))->save($save_is_success)) {
//            $this->error('修改 退单状态：通过[失败]');
//        }
    // 修改 订单表 中 订单的 退票 状态
//        $save_order_is_return = array('is_return' => 1);
//        $is_return = $model_order->where(array('id' => $order_id))->save($save_order_is_return);
//        if (!$is_return) {
//            $this->error('修改 订单状态：退票[失败]');
//        } else {
//            $this->success('退单处理成功！');
//        }
//    }

    function act_accept()
    {
        $id = I('get.id');
        if (!$id) $this->error('找不到退单！');
        $model_order_return = new OrderReturnModel();
        $info_order_return = $model_order_return->find($id);
        $order_id = $info_order_return['order_id'];

        $model_order = new OrderModel();
        $info_order = $model_order->find($order_id);

        vendor('Zhiyoubao.Zhiyoubao');
        $website = "http://plugin.slit.cn/";
        $zhiyoubao = new Zhiyoubao($website);

        $order_code = $info_order['sub_order_code'];
        $num = $info_order['num'];
        $req_return_ticket = $zhiyoubao->return_ticket($order_code, $num);
        $code = $req_return_ticket['code'];
        $description = $req_return_ticket['description'];
        $transactionName = $req_return_ticket['transactionName'];
        if ($transactionName != "RETURN_TICKET_NUM_RES" or $code != 0 or $description != "成功") {
            $this->error($description);
        } else {
            $this->success('退票成功！');
        }
    }

    function accept_piaofutong_return()
    {
        $order_return_id = I('get.id');
        if (!$order_return_id)
            $this->error('找不到订单');
        $model_order = new OrderSubModel();
        $model_order_return = new OrderReturnModel();
        $model_order_piaofutong = new OrderPiaofutongModel();

        $info_order_return = $model_order_return->find($order_return_id);
        $info_order = $model_order->find($info_order_return['order_id']);
        $info_order_piaofutong = $model_order_piaofutong->where(array('order_id' => $info_order_return['order_id']))->find();

        // 核销的数量
        $check_num = intval($info_order['check_num']);
        // 退票的数量
        $return_num = intval($info_order_return['num']);
        // 总数量
        $total_num = intval($info_order['num']);
        // 剩余数量
        $num = $total_num - $return_num;

        if ($num > $check_num)
            $this->error('票数超了，退你妹呢！');

        vendor('Piaofutong.Piaofutong');
        $piaofutong = new \Piaofutong();
        $req = $piaofutong->change_order($info_order_piaofutong['ordernum'], $num, $info_order['phone']);
        $success = $req['success'];
        if (!$success)
            $this->error($req['info']);
        $info = $req['info'];
        if ($info['done'] == '100')
            $this->success('退票成功！' . '手续费：' . $info['fee'] . '退款总额:' . $info['amount']);
        elseif ($info['done'] == '1095') {
            $this->success('需要退票审核，等待审核' . '手续费：' . $info['fee'] . '退款总额' . $info['amount']);
        } else {
            $this->error('退票失败！' . '手续费：' . $info['fee'] . '退款总额:' . $info['amount'] . '原因：' . $info['info'], true);
        }
    }

    function backOrderReturn()
    {
        $id = I('get.id');

        $model_order_return = new OrderReturnModel();

        $info_order_return = $model_order_return->find($id);
        if ($info_order_return['status']) $this->error('该订单已经处理，不可重复处理');
        $this->display();
    }

    function act_back_order_return()
    {
        if (IS_POST) {
            $data = I('post.');
            $id = $data['id'];
            $return_message = $data['return_message'];
            $model_order_return = new OrderReturnModel();

            $info_order_return = $model_order_return->find($id);
            if ($info_order_return['status']) $this->error('该订单已经处理，不可重复处理');

            $save_order_return = array(
                'return_message' => $return_message,
                'overtime' => time(),
                'status' => 1,
                'is_success' => -1
            );

            $is_order_return = $model_order_return->where(array('id' => $id))->save($save_order_return);
            $is_order_return
                ? $this->success('驳回成功！', U('index'))
                : $this->error('驳回失败！');
        }
    }
}