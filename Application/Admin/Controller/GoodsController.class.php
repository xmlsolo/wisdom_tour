<?php

namespace Admin\Controller;

use Common\Controller\AdminBaseController;
use Common\Model\GoodsModel;
use Common\Model\SellerModel;

class GoodsController extends AdminBaseController
{
    function index()
    {
        $model = new GoodsModel();
        $pageSize = 12;
        $where = array();
        $list = $this->displayPageList($model, $pageSize, $where);
        $this->assign('list', $list);
        $this->display();
    }

    function add()
    {
        if (IS_POST) {
            $data = $this->getData();
            $model = new GoodsModel();
            $data['price'] = floatval($data['price']);
            $data['addtime'] = time();
            $data['starttime'] = $data['starttime'] ? strtotime($data['starttime']) : time();
            $data['overtime'] = $data['overtime'] ? strtotime($data['overtime']) : time();
            $data['is_real'] = $this->getSwitch($data['is_real']);
            $data['is_play_date'] = $this->getSwitch($data['is_play_date']);
            $data['status'] = $this->getSwitch($data['status']);


            $this->createModel($model, $data);
            if (!$model->add($data)) {
                $this->error('添加失败！');
            } else {
                $this->success('添加成功！');
            }
            exit();
        }

        $model_seller = new SellerModel();
        $list_seller = $model_seller->select();
        $this->assign('list_seller', $list_seller);
        $this->display();
    }

    function delOne()
    {
        $id = $this->getId();
        if (!$id) $this->error('找不到商品！');
        $model_goods = new GoodsModel();
        $this->delOneById($model_goods, $id);
    }

    function editOne()
    {
        $id = $this->getId();
        $model_Goods = new GoodsModel();

        if (IS_POST) {
            $data = $this->getData();
            $data['price'] = $data['price'];
            $data['starttime'] = $data['starttime'] ? strtotime($data['starttime']) : time();
            $data['overtime'] = $data['overtime'] ? strtotime($data['overtime']) : time();
            $data['is_real'] = $this->getSwitch($data['is_real']);
            $data['is_play_date'] = $this->getSwitch($data['is_play_date']);
            $data['status'] = $this->getSwitch($data['status']);

            $model_Goods->where(array('id' => $id))->save($data) ? $this->success('修改成功！') : $this->error('修改失败！');
            exit();
        }
        $model_seller = new SellerModel();
        $list_seller = $model_seller->select();
        $this->assign('list_seller', $list_seller);
        $info = $model_Goods->find($id);
        $this->assign('info', $info);
        $this->display();
    }

    function update_pic()
    {
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize = 3145728;// 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->rootPath = './Uploads/'; // 设置附件上传根目录
        $upload->savePath = ''; // 设置附件上传（子）目录
        // 上传文件
        $info = $upload->upload();
        if (!$info) {// 上传错误提示错误信息
            $this->error($upload->getError());
        } else {// 上传成功
            $this->success($info);
        }
    }
}