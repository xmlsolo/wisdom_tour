<?php

namespace Admin\Controller;

use Think\Controller;

class LoginController extends Controller
{
    function index()
    {
//      票付通
//        vendor('Piaofutong.Piaofutong');
//        $piaofutong = new \Piaofutong();
        $this->display();
    }

    function login()
    {
        if (!IS_POST)
            $this->error('传输类型错误!');

        $data = I('post.');
        $username = $data['username'];
        $password = $data['password'];
        $vcode = $data['vcode'];

        if (!$this->check_verify($vcode)) {
            $this->error('验证码错误！');
        }

        if ($username != 'admin' and $password != '123456') {
            $this->error('用户名或密码错误！');
        }

        $this->sign();
    }

    function getVcode()
    {
        $config = array(
            'fontSize' => 30,    // 验证码字体大小
            'length' => 4,     // 验证码位数
            'useNoise' => false, // 关闭验证码杂点
        );
        $Verify = new \Think\Verify($config);
        $Verify->codeSet = '0123456789';
        $Verify->useImgBg = true;
        $Verify->entry();
    }

    private function check_verify($code, $id = '')
    {
        $verify = new \Think\Verify();
        return $verify->check($code, $id);
    }

    private function sign()
    {
        session('admin_id', 1);
        $this->success('登录成功', U('/Admin/index'));
    }
}
