<?php

namespace Admin\Controller;

use Common\Controller\AdminBaseController;
use Common\Model\SellerModel;

class SellerController extends AdminBaseController
{
    function index()
    {
        $model = new SellerModel();
        $list = $this->displayPageList($model, 12);
        $this->assign('list', $list);
        $this->display();
    }

    function editOne()
    {
        $id = $this->getId();
        $model = new SellerModel();
        if (IS_POST) {
            $save = $this->getData();
            $req = $model->where(array('id' => $id))->save($save);
            $req ? $this->success('修改成功！') : $this->error('修改失败！');
            exit();
        }

        $data = $model->getById($id);
        $this->assign('info', $data);
        $this->display();
    }

    function delOne()
    {
        $model = new SellerModel();
        $id = I('get.id');
        $this->delOneById($model, $id);
    }

    function add()
    {
        if (IS_POST) {
            $data = $this->getData();
            $model = new SellerModel();
            $req = $model->add($data);
            $req ? $this->success('添加成功！') : $this->error('添加失败！');
            exit();
        }
        $this->display();
    }

    function update_pic()
    {
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize = 3145728;// 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->rootPath = './Uploads/'; // 设置附件上传根目录
        $upload->savePath = ''; // 设置附件上传（子）目录
        // 上传文件
        $info = $upload->upload();
        if (!$info) {// 上传错误提示错误信息
            $this->error($upload->getError());
        } else {// 上传成功
            $this->success($info);
        }
    }
}