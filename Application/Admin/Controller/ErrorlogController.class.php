<?php

namespace Admin\Controller;

use Common\Controller\AdminBaseController;
use Common\Model\ErrorLogModel;
use Common\Model\GoodsModel;
use Common\Model\SellerModel;

class ErrorlogController extends AdminBaseController
{
    function index()
    {
        $model_error_log = new ErrorLogModel();
        $list = $this->displayPageList($model_error_log, 12, array(), 'id desc');
        $this->assign('list', $list);
        $this->display();
    }
}