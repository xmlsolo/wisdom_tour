<?php

namespace Notice\Controller;

use Common\Controller\NoticeBaseController;
use Common\Model\OrderModel;

class VerificationController extends NoticeBaseController
{
    function notice()
    {
        $model_order = new OrderModel();
        $data = $_GET;

        $order_code = $data['order_no'];
        $status = $data['status'];
        $checkNum=$data['checkNum'];
        if ($status == "check") {
//            增加 订单 核销票数
            $is_inc = $model_order->where(array('order_code'=>$order_code))->setInc('check_num',$checkNum);
            if(!$is_inc)
                $this->error('订单：'.$order_code.'核销票数增加失败',true);
            else
                exit('success');
        }
    }
}