<?php

namespace Notice\Controller;

use Common\Controller\NoticeBaseController;
use Common\Model\OrderModel;
use Common\Model\OrderReturnModel;
use Common\Model\OrderReturnPiaofutongModel;
use Common\Model\OrderSubModel;

class PiaofutongController extends NoticeBaseController
{
    private $verifyCode;
    private $piaofutong;

    function __construct()
    {
        parent::__construct();
        vendor('Piaofutong.Piaofutong');
        $this->piaofutong = new \Piaofutong();
        $this->verifyCode = $this->piaofutong->verifyCode;
    }

    function test()
    {
        $this->display();
    }

    /*
     *
        | VerifyCode   | 加密码            | string | 账号+密码的MD5值，请在业务执行前验证
        | Order16U     | 票付通订单号       | string |
        | OrderCall    | 远端订单号        | string |
        | ActionTime   | 执行时间          | string |
        | Tnumber      | 本次消费票数        | string | OrderState  =1/7：表示本次消费票数 ,=6: 表示取票数量 = 8：表示剩余可用票数，加上总消费数为实际订单数量 = 9：表示实际验证数量
        | OrderState   | 通知类型          | string | 1---全部消费通知 6---取票通知 7---部分消费通知 8---退款审核结果通知 9---特殊退款通知
        | AllCheckNum  | 总消费数          | string | （OrderState=1/7有此参数）
        | Source       | 渠道            | string | 2—内部；4--终端
        | Action       | 操作类型          | string | 1--有效期内消费 2--过期消费 3--修改/取消（验证前） 4--撤改/撤销（验证后） 5--供应商过期取消 6--取票
        | Refundtype   | 退款状态          | string | 1--成功2-- 拒绝（OrderState=8 有此参数）
        | Explain      | 审核说明          | string | （OrderState=8 有此参数）
        | RefundAmount | 退款金额（单价 * 数量） | string | （OrderState=8 有此参数）
        | RefundFee    | 退款手续费         | string | （OrderState=8 有此参数）
     *
     */

    function verification()
    {

        $str_post = file_get_contents('php://input');

        $arr_post = json_decode($str_post, true);

        if (!$arr_post)
            $this->error('大哥！不要闹！你的ip我收下了。');


        $verifyCode = $arr_post['VerifyCode'];  // 验证码
        $OrderCall = $arr_post['OrderCall'];    // 远端订单号

        $orderStatus = intval($arr_post['OrderState']); // 通知类型
        $arr_status = array(
            1 => '全部消费通知',
            6 => '取票通知',
            7 => '部分消费通知',
            8 => '退款审核结果通知',
            9 => '特殊退款通知'
        );
        $order_status_name = enumStatus($orderStatus, $arr_status, '没有类型');
        $Order16U = $arr_post['Order16U'];      // 票付通订单号
        $ActionTime = $arr_post['ActionTime'];  // 执行时间

        $Source = $arr_post['Source'];          // 渠道
        $arr_status = array(2 => '内部', 4 => '终端');
        $source_name = enumStatus($Source, $arr_status, '无渠道');
        $Action = $arr_post['Action'];          // 操作类型
        $arr_status = array(
            1 => '有效期内消费',
            2 => '过期消费',
            3 => '修改/取消（验证前）',
            4 => '撤改/撤销（验证后）',
            5 => '供应商过期取消',
            6 => '取票',
        );
        $action_name = enumStatus($Action, $arr_status, '无操作类型');
        $error_str = $this->format_error_str($OrderCall, $order_status_name, $action_name, $ActionTime, $source_name, $Order16U);
        // 验证
        if ($verifyCode != $this->verifyCode)
            $this->error($error_str . ' 验证码错误！' . json_decode($arr_post), true);

        $model_order = new OrderModel();
        $model_order_sub = new OrderSubModel();
        $where = array('order_code' => $OrderCall);
        switch ($orderStatus) {
            // 全部消费通知
            case 1:
                $Tnumber = $arr_post['Tnumber'];            // 表示本次消费票数
                $AllCheckNum = $arr_post['AllCheckNum'];    // 总消费数
                $save_sub_arr = array(
                    'check_num' => $AllCheckNum,
                    'dotime' => time(),
                    'is_status' => 1
                );

                $where = array('sub_order_code' => $OrderCall);
                $is_save = $model_order_sub->where($where)->save($save_sub_arr);
                if (!$is_save)
                    $this->error($error_str . '本次消费：' . $Tnumber . '总消费数：' . $AllCheckNum . '修改失败！', true);

                $info_order_sub = $model_order_sub->where($where)->find();
                $order_code = $info_order_sub['order_code'];
                $list_order_sub = $model_order_sub->where(array('order_code' => $order_code))->select();
                $arr_status = [];
                foreach ($list_order_sub as $order_sub) {
                    $arr_status[] = $order_sub['is_status'];
                }
                $info_order = $model_order->where(array('order_code' => $order_code))->find();
                $check_num = intval($info_order['check_num']) + intval($AllCheckNum);
                $save_order = array(
                    'check_num' => $check_num,
                    'dotime' => time()
                );
                $model_order->where(array('order_code' => $order_code))->save($save_order);

                if (!in_array(0, $arr_status)) {
                    $model_order->where(array('order_code' => $order_code))->save(array('status' => 1));
                } else {
                    $model_order->where(array('order_code' => $order_code))->save(array('status' => 2));
                }
                break;
            // 过期消费
            case 2:
                $Tnumber = $arr_post['Tnumber'];            // 表示本次消费票数
                $AllCheckNum = $arr_post['AllCheckNum'];    // 总消费数
                $save_sub_arr = array(
                    'check_num' => $AllCheckNum,
                    'dotime' => time(),
                    'is_status' => 1
                );

                $where = array('sub_order_code' => $OrderCall);
                $is_save = $model_order_sub->where($where)->save($save_sub_arr);
                if (!$is_save)
                    $this->error($error_str . '本次消费：' . $Tnumber . '总消费数：' . $AllCheckNum . '修改失败！', true);

                $info_order_sub = $model_order_sub->where($where)->find();
                $order_code = $info_order_sub['order_code'];
                $list_order_sub = $model_order_sub->where(array('order_code' => $order_code))->select();
                $arr_status = [];
                foreach ($list_order_sub as $order_sub) {
                    $arr_status[] = $order_sub['is_status'];
                }
                $info_order = $model_order->where(array('order_code' => $order_code))->find();
                $check_num = intval($info_order['check_num']) + intval($AllCheckNum);
                $save_order = array(
                    'check_num' => $check_num,
                    'dotime' => time()
                );
                $model_order->where(array('order_code' => $order_code))->save($save_order);

                if (!in_array(0, $arr_status)) {
                    $model_order->where(array('order_code' => $order_code))->save(array('status' => 1));
                } else {
                    $model_order->where(array('order_code' => $order_code))->save(array('status' => 2));
                }
                break;
            // 取票通知
            case 6:
                $Tnumber = $arr_post['Tnumber'];            // 表示取票数量
                $save_arr = array('tickets_num' => $Tnumber);
                $is_save = $model_order->where($where)->save($save_arr);
                if (!$is_save)
                    $this->error($error_str . ' 取票' . $Tnumber . ' 通知失败！', true);
                break;
            // 部分消费通知
            case 7:
                $Tnumber = $arr_post['Tnumber'];            // 表示本次消费票数
                $AllCheckNum = $arr_post['AllCheckNum'];    // 总消费数

//                $info_order = $model_order->where($where)->find();
                $save_arr = array(
//                    'check_num'=> intval($info_order['check_num']) + intval($Tnumber)
                    'dotime' => time(),
                    'check_num' => $AllCheckNum
                );
                $is_save = $model_order->where($where)->save($save_arr);
                if (!$is_save)
                    $this->error($error_str . '消费票数：' . $Tnumber . '总消费票数:' . $AllCheckNum . '修改失败！', true);
                break;
            // 退款审核结果通知
            case 8:
                $Tnumber = $arr_post['Tnumber'];            // 表示剩余可用票数，加上总消费数为实际订单数量
                $Refundtype = $arr_post['Refundtype'];      // 退款状态 1=>'成功',2=>'拒绝'
                $AllCheckNum = $arr_post['AllCheckNum'];    // 总消费数
                $RefundAmount = $arr_post['RefundAmount'];// 退款金额（单价 * 数量）
                $RefundFee = $arr_post['RefundFee'];    // 退款手续费
                $RemoteSn = $arr_post['RemoteSn'];    // 票付通退单号

                if (intval($Refundtype) == 1) {
                    $model_order_return_piaofutong = new OrderReturnPiaofutongModel();
                    $where_order_retnrn_piaofutong = array('piaofutong_order_id' => $Order16U);
                    $info_order_return_piaofutong = $model_order_return_piaofutong->where($where_order_retnrn_piaofutong)->find();
                    $where_order_return = array(
                        'order_id' => $info_order_return_piaofutong['order_id']
                    );
                    $save_order_return_piaofutong = array(
                        'action_time' => strtotime($ActionTime),
                        'num' => $Tnumber,
                        'checknum' => $AllCheckNum,
                        'amount' => $RefundAmount,
                        'fee' => $RefundFee,
                        'remote_sn' => $RemoteSn
                    );
                    $is_save_order_return_piaofutong = $model_order_return_piaofutong->where($where_order_retnrn_piaofutong)->save($save_order_return_piaofutong);
                    if (!$is_save_order_return_piaofutong)
                        $this->error($error_str . '票付通退单数据修改失败！', true);
                    // 修改 order_return 的数据
                    $model_order_return = new OrderReturnModel();
                    $save_order_return = array(
                        'is_success' => 1,
                        'return_message' => '剩余数量：' . $Tnumber . ' | 退票金额：' . $RefundAmount . ' | 手续费：' . $RefundFee,
                        'overtime' => time(),
                        'status' => 1,
                        'money' => $RefundAmount,
                        'charge' => $RefundFee
                    );

                    $is_order_return = $model_order_return->where($where_order_return)->save($save_order_return);
                    if (!$is_order_return)
                        $this->error($error_str . '原因：退票表 修改失败 | ' . '退款金额：' . $RefundAmount . ' | 手续费：' . $RefundFee, true);

                    $where_sub = array('sub_order_code' => $OrderCall);
                    $info_order_sub = $model_order_sub->where($where_sub)->find();
                    $order_code = $info_order_sub['order_code'];

                    $where = array('order_code' => $order_code);
                    $info_order = $model_order->where($where)->find();
                    $info_order_return = $model_order_return->where(array('order_id' => $info_order_sub['id']))->find();
                    $return_num = $info_order_return['num'];

                    // 修改 order_sub 的数据
                    $save_arr_sub = array(
                        'return_num' => $return_num,
                        'is_return' => 1,
                        'is_status' => 1
                    );
                    $is_order_save = $model_order_sub->where($where_sub)->save($save_arr_sub);

                    if (!$is_order_save)
                        $this->error($error_str . '原因：退票数量修改失败 | 退票数量：' . $return_num . ' | 剩余数量：' . $Tnumber . ' | 总消费数' . $AllCheckNum . ' | 退款金额(单价*数量)：' . $RefundAmount . ' | 手续费：' . $RefundFee, true);

                    // 判断订单的状态是否为完成
                    $save_status = array(
                        'return_num' => intval($info_order['return_num']) + intval($return_num)
                    );
                    $is_save_status = $model_order->where($where)->save($save_status);
                    if (!$is_save_status)
                        $this->error($error_str . '原因： 修改 订单 状态失败！', true);

//                    数据表中 的 退票数量 + 核销数量 = 票的总数
                    if (intval($info_order['check_num']) == intval($info_order['num'])) {
                        $save_status = array('status' => 1);
                        $is_save_status = $model_order->where($where)->save($save_status);
                    }else{
                        $save_status = array('status' => 2);
                        $is_save_status = $model_order->where($where)->save($save_status);
                    }

                    if (!$is_save_status)
                        $this->error($error_str . '原因： 修改 订单 状态失败！', true);

                    $_num = $info_order['return_num'];

                    $info_order = $model_order->where($where)->find();
                    if (intval($info_order['return_num']) + intval($info_order['check_num']) == intval($info_order['num'])) {
                        if (intval($info_order['check_num']) != 0) {
                            $save_status = array(
                                'status' => 2,
                                'return_num' => $info_order['num'] - $_num
                            );
                            $is_save_status = $model_order->where($where)->save($save_status);
                            if (!$is_save_status)
                                $this->error($error_str . '原因： 修改 订单 状态失败！', true);
                        } else {
                            $save_status = array(
                                'status' => 1,
                                'return_num' => $info_order['num'],
                                'is_return' => 1
                            );
                            $is_save_status = $model_order->where($where)->save($save_status);
                            if (!$is_save_status)
                                $this->error($error_str . '原因： 修改 订单 状态失败！', true);
                        }
                    }
                } else {
                    $this->error($error_str . '原因：退票被拒绝！', true);
                }
                break;
            // 特殊退款通知 (票务已经消费后，进行退票操作的处理)
            case 9:
                $Tnumber = $arr_post['Tnumber'];            // 表示实际验证数量
                break;
            default:
                // 你想多了，老老实实干活吧！
                $this->error($error_str . ' | 没有该状态回执', true);
        };
        echo 200;
        exit();
    }

    // 格式化错误输出
    private function format_error_str($order_code, $order_status_name, $action_name, $action_time, $source_name, $Order16U)
    {
        return '订单：' . $order_code . ' | 票付通订单号：' . $Order16U . ' | 操作：' . $order_status_name . ' | 动作：' . $action_name . ' | 渠道：' . $source_name . ' | 时间：' . $action_time . ' | ';
    }
}

// 枚举取出
function enumStatus($status, $arr_status, $err_msg = '没有应该状态')
{
    if (!array_key_exists($status, $arr_status))
        return $err_msg;
    return $arr_status[$status];
}