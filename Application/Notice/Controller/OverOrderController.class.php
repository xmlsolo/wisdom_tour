<?php

namespace Notice\Controller;

use Common\Controller\NoticeBaseController;
use Common\Model\OrderModel;

class OverOrderController extends NoticeBaseController
{
    function notice()
    {
        $model_order = new OrderModel();
        $data = $_GET;
        $this->errFile(json_encode($data));

        $order_code = $data['order_code'];
        $status = $data['status'];
        $return_num = $data['returnNum'];
        if ($status != "success")
            $this->error('订单' . $order_code . '完结失败！', true);

        // 修改订单状态为 核销
        $save_order = array(
            'is_status' => 1,
            'dotime' => time(),
            'returnNum' => $return_num
        );
        $is_save_order = $model_order->where(array('order_code' => $order_code))->save($save_order);
        if (!$is_save_order)
            $this->error('订单：' . $order_code . '完结处理失败', true);
        else
            exit('success');
    }
}