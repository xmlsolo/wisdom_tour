<?php

namespace Notice\Controller;


use Common\Model\ErrorLogModel;
use Common\Model\OrderModel;
use Common\Model\OrderReturnModel;
use Think\Controller;

class ReturnTicketController extends Controller
{
    function notice()
    {
        $model_order_return = new OrderReturnModel();
        $model_order = new OrderModel();
        $data = $_GET;

        $status = $data['auditStatus'];
        $return_num = $data['returnNum'];
        $return_order_code = $data['retreatBatchNo'];
        if ($status) {
            // 修改 退单表 处理时间 和 处理状态
            $save_order_return = array(
                'overtime' => time(),
                'status' => 1 // 处理过
            );
            if (!$model_order_return->where(array('return_order_code' => $return_order_code))->save($save_order_return)) {
                $this->error('修改退单表失败！');
            };

            //改变退票状态
            $save_is_success = array('is_success' => 1);
            if (!$model_order_return->where(array('return_order_code' => $return_order_code))->save($save_is_success)) {
                $this->error('修改 退单' . $return_order_code . '状态：通过[失败]', true);
            }
//             修改 订单表 中 订单的 退票 状态
            $info_order = $model_order_return->where(array('return_order_code' => $return_order_code))->find();
//            $save_order_is_return = array('is_return' => 1, 'dotime' => time());
//            $is_return = $model_order->where(array('id' => $info_order['order_id']))->save($save_order_is_return);
//            if (!$is_return) {
//                $this->error('修改 订单 ' . $info_order['order_id'] . ' 状态：退票[失败]');
//            }

            // 修改订单表中的退票数量
            $is_return_num = $model_order->where(array('id' => $info_order['order_id']))->setInc('return_num', $return_num);
            if (!$is_return_num)
                $this->error('订单：' . $info_order['order_id'] . ' 退票数量修改失败！', true);
            else {
                exit('success');
            }
        } else {
            $this->error('退单号：' . $return_order_code . '为非退单状态，无法进行退单操作！');
        }
        exit();
    }

    //    复写 error
    protected function error($message = '', $is_log = false, $jumpUrl = '', $ajax = false)
    {
        if ($is_log) {
            $model_error_log = new ErrorLogModel();
            $data = array(
                'addtime' => time(),
                'controller' => CONTROLLER_NAME,
                'action' => ACTION_NAME,
                'msg' => $message
            );
            if (!$model_error_log->add($data))
                $this->errFile($message);
        }
        parent::error($message, $jumpUrl, $ajax);
    }

    private function errFile($msg)
    {
        $myfile = fopen("errFile.txt", "w");
        fwrite($myfile, $msg);
        fclose($myfile);
    }
}