<?php

namespace Vendor\Zhiyoubao\Zhiyoubao;

class Zhiyoubao
{
    public $tickets_type_sn;
    public $website_url;
    private $username;
    private $corpCode;
    private $url;
    public $is_request;

    function __construct($website, $tickets_type_sn = TicketTypeSn::DEFAULT_SN, $corpCode = 'TESTFX', $username = "admin")
    {
        $this->url = "http://zyb-zff.sendinfo.com.cn/boss/service/code.htm";
        $this->tickets_type_sn = $tickets_type_sn;
        $this->corpCode = $corpCode;
        $this->username = $username;
        $this->website_url = $website;
    }

    static function PAY_TYPE()
    {
        return [1 => 'spot', 2 => 'vm', 3 => 'zyb'];
    }

    static function PAY_TYPE_CN()
    {
        return [1 => '现场支付', 2 => '备佣金', 3 => '智游宝'];
    }

    // 生成订单号
    function getOrderCode()
    {
        return date('YmdHis', time()) . random_string(4);
    }

    /*
     * 生成子订单号
     * $order_code  主订单号
     * $num         订单号 后 随机几位数字 | 0 为再次生成订单号
    */
    function getSubOrderCode($order_code, $num = 3)
    {
        if (!$order_code)
            return '必须 - 主订单号';
        if ($num != 0)
            return $order_code . '-' . random_string($num);
        else
            return $this->getOrderCode();
    }

    /*
     * 获取二维码链接
     * $orderCode   订单号
     */
    function getQr($orderCode)
    {
        $nowTime = date('Y-m-d H:i:s', time());
        $transaction = DataTransaction::QUERY_IMG_URL_REQ;
        $xml =
            '<?xml version="1.0" encoding="UTF-8"?>
            <PWBRequest>
                <transactionName>' . $transaction . '</transactionName>
                    <header>
                        <application>SendCode</application>
                        <requestTime>' . $nowTime . '</requestTime>
                    </header>
                <identityInfo>
                    <corpCode>' . $this->corpCode . '</corpCode>
                    <userName>' . $this->username . '</userName>
                </identityInfo>
                <orderRequest>
                    <order>
                        <orderCode>' . $orderCode . '</orderCode>
                    </order>
                </orderRequest>	
            </PWBRequest>';
        return $this->request($xml);
    }

    /*
     * 下单
     * $certificateNo   身份证号
     * $linkName        联系姓名
     * $linkMobile      联系电话
     * $main_orderCode  主订单号
     * $orderPrice      订单金额
     *
     * $sub_orderCode   子订单号
     * $price           单价
     * $num             数量
     * $totalPrice      总额
     * $playDate        游玩时间
     * $goodsName       商品名称
     * $payMethod       支付方式 -> 默认【third_vm】 -> 第三方支付
     */
    function addOrder($certificateNo, $linkName, $linkMobile, $main_orderCode, $orderPrice, $sub_orderCode, $price, $num, $totalPrice, $playDate, $goodsName, $payMethod)
    {
        $transaction = DataTransaction::SEND_CODE_REQ;
        $nowTime = date('Y-m-d', time());
        $xml =
            "<PWBRequest>
                <transactionName>" . $transaction . "</transactionName>
                <header>
                    <application>SendCode</application>
                    <requestTime>" . $nowTime . "</requestTime>
                </header>
                <identityInfo>
                    <corpCode>" . $this->corpCode . "</corpCode>
                    <userName>" . $this->username . "</userName>
                </identityInfo>
                <orderRequest>
                    <order>
                        <certificateNo>$certificateNo</certificateNo>
                        <linkName>$linkName</linkName>
                        <linkMobile>$linkMobile</linkMobile>
                        <orderCode>$main_orderCode</orderCode>
                        <orderPrice>$orderPrice</orderPrice>
                        <payMethod>$payMethod</payMethod>
                        <scenicOrders>
                            <scenicOrder>
                                <orderCode>$sub_orderCode</orderCode>
                                <price>$price</price>
                                <quantity>$num</quantity>
                                <totalPrice>$totalPrice</totalPrice>
                                <occDate>$playDate</occDate>
                                <goodsCode>" . $this->tickets_type_sn . "</goodsCode>
                                <goodsName>$goodsName</goodsName>
                            </scenicOrder>
                        </scenicOrders>
                    </order>
                </orderRequest>
            </PWBRequest>";
        return $this->request($xml);
    }

    /*
     *  退单
     * $sub_order_code      子订单号
     * $num                 退票数量
     * $return_order_code   第三方退单号
     * $idcards             如果是实名制订单退票，请带上身份证号码多个身份证号中间用  “,”分割
     */

    function return_ticket($order_code, $num)
    {
        $xmlMsg = "
        <PWBRequest>
          <transactionName>" . DataTransaction::RETURN_TICKET . "</transactionName>
          <header>
            <application>SendCode</application>
            <requestTime>" . date('Y-m-d', time()) . "</requestTime>
          </header>
        <identityInfo>
            <corpCode>" . $this->corpCode . "</corpCode>
            <userName>" . $this->username . "</userName>
          </identityInfo>
          <orderRequest>
            <returnTicket>
              <orderCode>" . $order_code . "</orderCode>
              <orderType>scenic</orderType>
              <returnNum>" . $num . "</returnNum>
            </returnTicket>
          </orderRequest>
        </PWBRequest>";
//        return $num;
        $key = md5("xmlMsg=" . $xmlMsg . "," . $this->corpCode);
        $n = "xmlMsg=" . $xmlMsg;
        $new = $n . ",&sign=" . $key;
        $res = xml2Array(Sendxml($this->url, $new));
        return $res;
//        return $this->request($xmlMsg);
    }


    private function request($xml)
    {
        $req = array();
        $key = $this->formatUrl($xml);
        $_arr = xml2Array(Sendxml($this->url, $key));
        $code = $_arr['code'];
        if ($code) {
            $req['status'] = 0;
            $req['info'] = $_arr['description'];
        } else {
            $req['status'] = 1;
            $req['info'] = $_arr;
        }
        return $req;
    }

    private function formatUrl($xmlMsg)
    {
        $key = md5("xmlMsg=" . $xmlMsg . "," . $this->corpCode);

        $n = "xmlMsg=" . $xmlMsg;
        $new = $n . ",&sign=" . $key;
        return $new;
    }

}

class TicketTypeSn
{
    const DEFAULT_SN = "PST20180111016903";
}

class ApiType
{
//    通知
    const TONGZHI_DAOFUDINGDANQUEREN = 3; // 到付订单确认通知
    const TONGZHI_DINGDANWANJIE = 12; // 订单完结通知
    const TONGZHI_TUIPIAO = 14; // 退票通知
    const TONGZHI_HEXIAO = 18; // 核销通知

//    查询
    const CHAXUN_DAOFUDINGDAN = 5; // 到付订单查询
    const CHAXUN_JIANPIAO = 8; // 查询检票情况
    const CHAXUN_FAMATUPIAN = 10; // 发码图片查询
    const CHAXUN_DINGDAN = 13; // 订单查询
    const CHAXUN_TUIPIAOQINGKUANG = 17; // 退票情况查询

//    二维码
    const ERWEIMA = 1; // 获取二维码链接
    const ERWEIMA_DUAN = 4; // 获取二维码短链接

//    取消
    const QUXIAO_DINGDAN = 9; // 取消订单
    const QUXIAO_DAOFUDAN = 16; // 到付单取消

//    其他
    const XIADAN = 7; // 下单请求
    const FADUANXIN = 11; // 发短信

    const FENXIAOSHANGGAIQIAN = 2; // 分销商改签
    const BUFENTUIPIAO = 6; // 部分退票接口
    const HUOQUDINGDANJIANPIAO = 15; // 获取订单检票信息
}

class DataTransaction
{
    const SEND_CODE_REQ = "SEND_CODE_REQ"; // 下单请求
    const RETURN_TICKET = "RETURN_TICKET_NUM_REQ"; // 退票
    const QUERY_IMG_URL_REQ = "QUERY_IMG_URL_REQ"; // 获取二维码链接
    const ORDER_ENDORSE_REQ = "ORDER_ENDORSE_REQ"; // 分销商改签
    const QUERY_SHORT_IMG_URL_REQ = "QUERY_SHORT_IMG_URL_REQ"; // 获取二维码短链接
    const QUERY_SPOT_ORDER_REQ = "QUERY_SPOT_ORDER_REQ"; // 到付订单查询
    const RETURN_TICKET_NUM_NEW_REQ = "RETURN_TICKET_NUM_NEW_REQ"; // 部分退票接口
    const CHECK_STATUS_QUERY_REQ = "CHECK_STATUS_QUERY_REQ"; // 查询检票情况
    const SEND_CODE_CANCEL_NEW_REQ = "SEND_CODE_CANCEL_NEW_REQ"; // 取消订单
    const SEND_CODE_IMG_REQ = "SEND_CODE_IMG_REQ"; // 发码图片查询
    const SEND_SM_REQ = "SEND_SM_REQ"; // 发短信
    const QUERY_ORDER_REQ = "QUERY_ORDER_REQ"; // 订单查询
    const QUERY_SUB_ORDER_CHECK_RECORD_REQ = "QUERY_SUB_ORDER_CHECK_RECORD_REQ"; // 获取订单检票信息
    const CANCEL_SPOT_ORDER_REQ = "CANCEL_SPOT_ORDER_REQ"; // 到付单取消
    const QUERY_RETREAT_STATUS_REQ = "QUERY_RETREAT_STATUS_REQ"; // 退票情况查询
}

/*发送xml*/
function Sendxml($url, $new)
{
    $ch = curl_init();
    $header = "Content - type: text / xml; charset = utf - 8";
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $new);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, $header);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

//将XML转为array
function xml2Array($xml)
{
    libxml_disable_entity_loader(true);
    $values = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    return $values;
}

//数组转XML
function array2Xml($arr)
{
    $xml = " < xml>";
    foreach ($arr as $key => $val) {
        if (is_numeric($val)) {
            $xml .= " < " . $key . ">" . $val . " </" . $key . " > ";
        } else {
            $xml .= "<" . $key . " ><![CDATA[" . $val . "]] ></" . $key . " > ";
        }
    }
    $xml .= "</xml > ";
    return $xml;
}

// 随机字符串
function random_string($length = 8)
{
    $password = '';
    $chars = '1234567890';
    for ($i = 0; $i < $length; $i++) {
        $password .= $chars[mt_rand(0, strlen($chars) - 1)];
    }
    return $password;
}