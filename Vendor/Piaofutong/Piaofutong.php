<?php

/*
 * 产品类型
 * A 景点B 线路C 酒店F 套票
 */

// 方法名称
class ActionName
{
    const SCENIC_SPOT_LIST = 'Get_ScenicSpot_List';    // 查询景区列表
    const SCENIC_SPOT_INFO = 'Get_ScenicSpot_Info';    // 查询景区详情信息
    const TICKET_LIST = 'Get_Ticket_List';             // 查询门票列表
    const REAL_TIME_STORAGE = 'GetRealTimeStorage';    // 动态价格
    const CHECK_PERSON_ID = 'Check_PersonID';          // 身份证校验接口
    const ORDER_SUBMIT = 'PFT_Order_Submit';           // 提交订单
    const ORDER_CHANGE = 'Order_Change_Pro';           // 修改/取消订单
}


class Ptype
{
    const A = '景点';
    const B = '线路';
    const C = '酒店';
    const F = '套票';
}

class Piaofutong
{
    private $account;               // 账号
    private $secret;                // 密钥
    private $url;                   // 接口地址
    private $test_lid;         // 测试商品ID
    private $test_tid;  // 测试门票ID
    private $test_m;           // 测试供应商ID

    public $verifyCode;           // 验证码
    public $request;
    protected $error;

    const SOAP_ERROE = '票付通 信息获取 失败！';


    function __construct()
    {
        $this->error = '';
        $this->request = array('success' => 0, 'info' => $this->error);

        // 测试数据
        $this->test_lid = 2633;
        $this->test_tid = 5715;
        $this->test_m = 113;

        $this->url = 'http://open.12301.cc/openService/MXSE.wsdl';
        $this->account = '270156';
        $this->secret = 'dfca25ea3647d4bd686d8b80ea4f86bf';
//        $this->verifyCode = 'ab54f9d58e7774b14750ff468ce25a67';
        $this->verifyCode = '15d70c5f76956ae269445d44bfc2e843';

        // 测试接口
//        $this->url = 'http://open.12301dev.com/openService/MXSE_beta.wsdl';
//        $this->account = '100019';
//        $this->secret = 'jjl4yk11f82ce6c0f33a5c003f2fec56';
//        $this->verifyCode = md5($this->piaofutong->account . $this->piaofutong->secret);
    }

    /*
     * 获取 景区列表
     * $start 必须为 $size 的倍数 ， 例如 size = 12，那么start 为 0 12 24 36 48 52 64
     */
    function scenicSpotList($start = 0, $size = 12)
    {
        $parm = array('n' => $start, 'm' => $size);
        $req = $this->call_function(ActionName::SCENIC_SPOT_LIST, $parm);
        $req_list = $this->verify($req);
        if (!$req_list['success']) {
            $this->request['success'] = 0;
            $this->request['info'] = $req_list['info'];
            return $this->request;
        }

        $list = array();
        foreach ($req_list['info'] as $item) {
            $_arr['uuid'] = $item['UUid'];
            $_arr['area'] = $item['UUarea'];
            $_arr['addtime'] = $item['UUaddtime'];
            $_arr['thumb'] = $item['UUimgpath'];
            $_arr['name'] = $item['UUtitle'];
            $_arr['type'] = $item['UUp_type'];
            $_arr['model'] = 'piaofutong';
            $list[] = $_arr;
        }
        $this->request['success'] = 1;
        $this->request['info'] = $list;
        return $this->request;
    }

    /*
     * 获取 景区详细信息
     * $uuid 景区ID
     */
    function scenicSpotInfo($uuid)
    {
        $parm = array('n' => $uuid);
        $req = $this->call_function(ActionName::SCENIC_SPOT_INFO, $parm);
        $req_info = $this->verify($req);
        if (!$req_info['success']) {
            $this->request['success'] = 0;
            $this->request['info'] = $req_info['info'];
            return $this->request;
        }

        $scenic_info = $req_info['info'];

        $_arr['uuid'] = $scenic_info['UUid'];                       // 景区id
        $_arr['title'] = $scenic_info['UUtitle'];
        $_arr['area'] = $scenic_info['UUarea'];
        $_arr['address'] = $scenic_info['UUaddress'];
        $_arr['thumb'] = $scenic_info['UUimgpath'];
        $_arr['p_type'] = $scenic_info['UUp_type'];

        $_arr['bhjq'] = $scenic_info['UUbhjq'];                       // 景点介绍
        $_arr['j_type'] = $scenic_info['UUjtype'];                    // 景区级别
        $_arr['lng_lat_pos'] = $scenic_info['UUlng_lat_pos'];           // 景区维度
        $_arr['salerid'] = $scenic_info['UUsalerid'];                // 商家编号      ( 查询订单时使用此编号)
        $_arr['tel'] = $scenic_info['UUtel'];                          // 景区联系电话
        $_arr['jqts'] = $scenic_info['UUjqts'];                        // 景区相关提示
        $_arr['runtime'] = $scenic_info['UUruntime'];                 // 景区营业时间
        $_arr['fax'] = $scenic_info['UUfax'];                          // 景区联系传真
        $_arr['status'] = $scenic_info['UUstatus'];                   // 景区在售状态 (1 在售，2 下架，3 删除)
        $_arr['topics'] = $scenic_info['UUtopics'];                   // 旅游主题
        $_arr['jtzn'] = $scenic_info['UUjtzn'];                        // 公共交通 与 自驾线路
        $_arr['opentime'] = $scenic_info['UUopentime'];               // 景区开业时间

        $this->request['success'] = 1;
        $this->request['info'] = $_arr;
        return $this->request;
    }

    /*
     * 查询 门票列表
     * $uuid 景区Id (可为空，为空时 参数2 不能为空)
     * $ticket_id 门票Id(为空时列出景区下面所有的门票)
     */
    function get_ticket_list($uuid = '', $ticket_id = '', $is_list)
    {
        $parm = array('n' => $uuid, 'm' => $ticket_id);
        $req = $this->call_function(ActionName::TICKET_LIST, $parm, $is_list);
        $req_list = $this->verify($req);
        if (!$req_list['success']) {
            $this->request['success'] = 0;
            $this->request['info'] = $req_list['info'];
            return $this->request;
        }

        $list_info = $req_list['info'];

        $_arr = array();
        foreach ($list_info as $item) {
            $_arr['uuid'] = $item['UUid'];      // 门票Id
            $_arr['lid'] = $item['UUlid'];      // 景区id
            $_arr['title'] = $item['UUtitle'];
            $_arr['status'] = $item['UUstatus']; // 景区在售状态 1 在售，2 下架，3删除
            $_arr['tprice'] = $item['UUtprice']; // 市场价（元）
            $_arr['delaydays'] = $item['UUdelaydays'];              // 有效天数

            $_arr['aid'] = $item['UUaid'];      // 供应商id
            $_arr['pid'] = $item['UUpid'];      // 产品id

            $_arr['ass_station'] = $item['UUass_station'];      // 集合地点
            $_arr['notes'] = $item['UUnotes'];                        // 购票须知

            $_arr['age_limit_max'] = $item['UUage_limit_max'];      // 购票最大年龄 限制[不足多少岁后可以购买]
            $_arr['age_limit_min'] = $item['UUage_limit_min'];      // 购票最小年龄 限制[满多少岁后可以购买]
            $_arr['buy_limit_num'] = $item['UUbuy_limit_num'];      // 限购票数（数字大于0的数字）
            $_arr['buy_limit_date'] = $item['UUbuy_limit_date'];    // 购买限制周期 0:整个销售时间段,1:每日,2:每周,3:每月，4:身份整限购每张，5:手机号+身份证每张，6:手机号+身份证每笔
            $_arr['buy_limit'] = $item['UUbuy_limit'];               // 购买限制类型 0:不限,1:手机号,2:身份证,3:手机号+身份证
            $_arr['buy_limit_up'] = $item['UUbuy_limit_up'];        // 最大购买数量，0代表不限
            $_arr['buy_limit_low'] = $item['UUbuy_limit_low'];      // 最小购买数量
            $_arr['expire_action'] = $item['UUexpire_action'];      // 订单过期后处理方式 1=不做处理，2=自动完结，3=自动验证，4=自动取消
            $_arr['expire_action_days'] = $item['UUexpire_action_days']; // 多少天后自动处理 - 结合expire_action使用
            $_arr['expire_cancel_fee'] = $item['UUexpire_cancel_fee']; // 自动取消的手续费
            $_arr['getaddr'] = $item['UUgetaddr'];                    // 取票信息
            $_arr['tourist_info'] = $item['UUtourist_info'];        // 游客信息 0不需要填写1需要填写 2需要填写所有游客身份证
            $_arr['cancel_auto_onMin'] = $item['UUcancel_auto_onMin'];  // 多少分支未支付自动取消
            $_arr['num_modify'] = $item['UUnum_modify'];            // 修改人数限制 0只可减少，大于0为增减或减少的上限比例
            $_arr['pay'] = $item['UUpay'];                              // 支付方式 0现场支付1在线支付

            $_arr['startplace'] = $item['UUstartplace'];            // 出发城市或地区 （线路）
            $_arr['endplace'] = $item['UUendplace'];                  // 目的地（线路）
            $_arr['rdays'] = $item['UUrdays'];                          // 一次游玩持续天数（线路）
            $_arr['series_model'] = $item['UUseries_model'];        // 团号模型（XXX{年月日131212}XXX（线路））

            $_arr['ddays'] = $item['UUddays'];                          // 提前预定天数
            $_arr['dhour'] = $item['UUdhour'];                          // 提前预定截止时间

            $_arr['max_order_days'] = $item['UUmax_order_days'];    // 提前下单最多间隔天数
            $_arr['print_ticket_limit'] = $item['UUprint_ticket_limit'];// 取票时间，游玩当日可取

            $_arr['refund_audit'] = $item['UUrefund_audit'];        // 退票审核 0 不审核 1 需审核
            $_arr['refund_early_time'] = $item['UUrefund_early_time'];  // 退票提前多少分钟
            $_arr['refund_rule'] = $item['UUrefund_rule'];          // 退票规则 0游玩日期内可退，1游玩日期前可退需在验证截止时间前退，2不可退

            $_arr['cancel_cost'] = $item['UUcancel_cost'];          // 取消费用二维数组 c_type=类型 0 固定金额 1百分比,c_days=游玩前天数,c_cost=
            $_arr['reb_type'] = $item['UUreb_type'];                // 取消费用类型 0 百分比，1 实际指定具体值
            $_arr['shop'] = $item['UUshop'];                          // 可以销售渠道

            $_arr['delaytype'] = $item['UUdelaytype'];              // 订单有效期类型 0：游玩日期后X天有效，1：下单日期后X天有效2：设定时间段内有效，3：游玩日期当天有效
            $_arr['order_start'] = $item['UUorder_start'];          // 订单有效期开始日期00:00:00
            $_arr['order_end'] = $item['UUorder_end'];              // 截止订单有效期日期 到当天的23：59：59

            $_arr['if_verify'] = $item['UUif_verify'];              //  游玩日期不可验证 1：不可验证，0:可以验证
            $_arr['order_limit'] = $item['UUorder_limit'];         // 订单验证限制 日期 默认全部  0,6 代表周日周六不可验证
            $_arr['batch_day_check'] = $item['UUbatch_day_check'];// 每单每天可验证张数 验证方式=1（分批验证）时必填，0不限
            $_arr['v_time_limit'] = $item['UUv_time_limit'];        // 验证时间 08:00
            $_arr['batch_check'] = $item['UUbatch_check'];          // 分批验证 0 不支持 1 支持，2：一票一码，3：一票一证
            $_arr['delaytime'] = $item['UUdelaytime'];              // 延迟验证
            $list[] = $_arr;
        }
        $this->request['success'] = 1;
        $this->request['info'] = $list;
        return $this->request;
    }

    /*
     * 查询 门票
     * $ticket_id 门票Id(为空时列出景区下面所有的门票)
     */
    function get_ticket($ticket_id)
    {
        $parm = array('m' => $ticket_id);
        $req_info = $this->call_function(ActionName::TICKET_LIST, $parm);
        $req_info = $this->verify($req_info);
        $info = $req_info['info'];
        if (!$req_info['success']) {
            $this->request['success'] = 0;
            $this->request['info'] = $info['info'];
            return $this->request;
        }
        $_arr['uuid'] = $info['UUid'];      // 门票Id
        $_arr['lid'] = $info['UUlid'];      // 景区id
        $_arr['aid'] = $info['UUaid'];      // 供应商id
        $_arr['pid'] = $info['UUpid'];      // 产品id

        $_arr['title'] = $info['UUtitle'];
        $_arr['status'] = $info['UUstatus']; // 景区在售状态 1 在售，2 下架，3删除
        $_arr['tprice'] = $info['UUtprice']; // 市场价（元）
        $_arr['lprice'] = $info['UUlprice']; // 市场价（元）
        $_arr['delaydays'] = $info['UUdelaydays'];              // 有效天数

        $_arr['ass_station'] = $info['UUass_station'];      // 集合地点
        $_arr['notes'] = $info['UUnotes'];                        // 购票须知

        $_arr['age_limit_max'] = $info['UUage_limit_max'];      // 购票最大年龄 限制[不足多少岁后可以购买]
        $_arr['age_limit_min'] = $info['UUage_limit_min'];      // 购票最小年龄 限制[满多少岁后可以购买]
        $_arr['buy_limit_num'] = $info['UUbuy_limit_num'];      // 限购票数（数字大于0的数字）
        $_arr['buy_limit_date'] = $info['UUbuy_limit_date'];    // 购买限制周期 0:整个销售时间段,1:每日,2:每周,3:每月，4:身份整限购每张，5:手机号+身份证每张，6:手机号+身份证每笔
        $_arr['buy_limit'] = $info['UUbuy_limit'];               // 购买限制类型 0:不限,1:手机号,2:身份证,3:手机号+身份证
        $_arr['buy_limit_up'] = $info['UUbuy_limit_up'];        // 最大购买数量，0代表不限
        $_arr['buy_limit_low'] = $info['UUbuy_limit_low'];      // 最小购买数量
        $_arr['expire_action'] = $info['UUexpire_action'];      // 订单过期后处理方式 1=不做处理，2=自动完结，3=自动验证，4=自动取消
        $_arr['expire_action_days'] = $info['UUexpire_action_days']; // 多少天后自动处理 - 结合expire_action使用
        $_arr['expire_cancel_fee'] = $info['UUexpire_cancel_fee']; // 自动取消的手续费
        $_arr['getaddr'] = $info['UUgetaddr'];                    // 取票信息
        $_arr['tourist_info'] = $info['UUtourist_info'];        // 游客信息 0不需要填写1需要填写 2需要填写所有游客身份证
        $_arr['cancel_auto_onMin'] = $info['UUcancel_auto_onMin'];  // 多少分支未支付自动取消
        $_arr['num_modify'] = $info['UUnum_modify'];            // 修改人数限制 0只可减少，大于0为增减或减少的上限比例
        $_arr['pay'] = $info['UUpay'];                              // 支付方式 0现场支付1在线支付

        $_arr['startplace'] = $info['UUstartplace'];            // 出发城市或地区 （线路）
        $_arr['endplace'] = $info['UUendplace'];                  // 目的地（线路）
        $_arr['rdays'] = $info['UUrdays'];                          // 一次游玩持续天数（线路）
        $_arr['series_model'] = $info['UUseries_model'];        // 团号模型（XXX{年月日131212}XXX（线路））

        $_arr['ddays'] = $info['UUddays'];                          // 提前预定天数
        $_arr['dhour'] = $info['UUdhour'];                          // 提前预定截止时间

        $_arr['max_order_days'] = $info['UUmax_order_days'];    // 提前下单最多间隔天数
        $_arr['print_ticket_limit'] = $info['UUprint_ticket_limit'];// 取票时间，游玩当日可取

        $_arr['refund_audit'] = $info['UUrefund_audit'];        // 退票审核 0 不审核 1 需审核
        $_arr['refund_early_time'] = $info['UUrefund_early_time'];  // 退票提前多少分钟
        $_arr['refund_rule'] = $info['UUrefund_rule'];          // 退票规则 0游玩日期内可退，1游玩日期前可退需在验证截止时间前退，2不可退

        $_arr['cancel_cost'] = $info['UUcancel_cost'];          // 取消费用二维数组 c_type=类型 0 固定金额 1百分比,c_days=游玩前天数,c_cost=
        $_arr['reb_type'] = $info['UUreb_type'];                // 取消费用类型 0 百分比，1 实际指定具体值
        $_arr['shop'] = $info['UUshop'];                          // 可以销售渠道

        $_arr['delaytype'] = $info['UUdelaytype'];              // 订单有效期类型 0：游玩日期后X天有效，1：下单日期后X天有效2：设定时间段内有效，3：游玩日期当天有效
        $_arr['order_start'] = $info['UUorder_start'];          // 订单有效期开始日期00:00:00
        $_arr['order_end'] = $info['UUorder_end'];              // 截止订单有效期日期 到当天的23：59：59

        $_arr['if_verify'] = $info['UUif_verify'];              //  游玩日期不可验证 1：不可验证，0:可以验证
        $_arr['order_limit'] = $info['UUorder_limit'];         // 订单验证限制 日期 默认全部  0,6 代表周日周六不可验证
        $_arr['batch_day_check'] = $info['UUbatch_day_check']; // 每单每天可验证张数 验证方式=1（分批验证）时必填，0不限
        $_arr['v_time_limit'] = $info['UUv_time_limit'];        // 验证时间 08:00
        $_arr['batch_check'] = $info['UUbatch_check'];          // 分批验证 0 不支持 1 支持，2：一票一码，3：一票一证
        $_arr['delaytime'] = $info['UUdelaytime'];              // 延迟验证

        return $_arr;
    }

    /*
     * 动态价格
     *  | aid        | 供应商id  | int    |                              |
        | uuid        | 产品id   | int    | 注：Get_Ticket_List 查询出来的UUpid |
        | start_date | 有效开始时间 | string | 格式2013-11-11                 |
        | end_date   | 有效结束时间 | string | 格式2013-11-11
     */
    function get_real_time_storage($aid, $uuid, $start_date, $end_date, $is_list = false)
    {
        $parm = array('aid' => $aid, 'pid' => $uuid, 'start_date' => $start_date, 'end_date' => $end_date);
        $req = $this->call_function(ActionName::REAL_TIME_STORAGE, $parm, $is_list);
        $req_info = $this->verify($req, 'items');
        if (!$req_info['success']) {
            $this->request['success'] = 0;
            $this->request['info'] = $req_info['info'];
            return $this->request;
        }
        $price_info = $req_info['info'];

        $arr = array();
        if ($is_list) {
            foreach ($price_info as $item) {
                $_arr = array();
                $_arr['date'] = $item['date'];              // 总库存 -1 表示无限
                $_arr['storage'] = $item['storage'];              // 总库存 -1 表示无限
                $_arr['remain'] = $item['remain'];                // 实时库存 9999999 表示无限
                $_arr['buy_price'] = $item['buy_price'];          // 结算价 （分）
                $_arr['retail_price'] = $item['retail_price'];    // 零售价 （分）
                $arr[] = $_arr;
            }
        } else {
            $arr['date'] = $price_info['date'];              // 总库存 -1 表示无限
            $arr['storage'] = $price_info['storage'];              // 总库存 -1 表示无限
            $arr['remain'] = $price_info['remain'];                // 实时库存 9999999 表示无限
            $arr['buy_price'] = $price_info['buy_price'];          // 结算价 （分）
            $arr['retail_price'] = $price_info['retail_price'];    // 零售价 （分）
        }
        $this->request['success'] = 1;
        $this->request['info'] = $arr;
        return $this->request;
    }

    /*
     * personId | 身份证号 | string |      |
     */
    function check_personID($personId)
    {
        $parm = array('personId' => $personId);
        $req = $this->call_function(ActionName::CHECK_PERSON_ID, $parm);
        $success = $req['success'];
        if (!$success) {
            $this->request['success'] = 0;
            $this->request['info'] = Piaofutong::SOAP_ERROE;
        } else {
            $rec = $req['info']['Rec'];
            $code = $rec['UUdone'];
            if ($code == '100') {
                $this->request['success'] = 1;
                $this->request['info'] = '身份证验证成功！';
            } else {
                $this->request['success'] = 0;
                $this->request['info'] = $rec['UUerrorinfo'];
            }
        }
        return $this->request;
    }

    /*
     * #### 提交订单：PFT_Order_Submit
    - 请求参数说明
    | 参数名称       | 中文名称     | 类型     | 说明                                       |
    | :--------- | :------- | :----- | :--------------------------------------- |
    | ac         | 账号       | string |                                          |
    | pw         | 账号       | string |                                          |
    | lid        | 景区id     | string |                                          |
    | tid        | 门票id     | string |                                          |
    | remotenum  | 远端订单号    | string | 贵网站的唯一订单号，请确保唯一，不能为空                     |
    | tprice     | 结算价      | string | 供应商给你们的结算价，单位：分                          |
    | tnum       | 数量       | string |                                          |
    | playtime   | 游玩日期     | string | 例：2012-03-16                             |
    | ordername  | 取票人姓名    | string | 多个请用英文逗号隔开，与personID配合使用                 |
    | ordertel   | 取票人手机号   | string |                                          |
    | contactTEL | 联系人手机号   | string |                                          |
    | smsSend    | 是否需要发送短信 | string | （0 发送1 不发送注：发短信只会返回双方订单号，不发短信才会将凭证信息返回   |
    | paymode    | 扣款方式     | string | （0 使用账户余额 2 使用供应商授信支付 4 现场支付注：余额不足返回错误122 |
    | ordermode  | 下单方式     | string | 0 正常下单                                   |
    | assembly   | 集合地点     | string | 线路时需要，可为空                                |
    | series     | 团号       | string | 线路时需要，可为空；场次信息获取接口详情3.7 方法。必填参数格式：json_encode(array(（int）场馆id,（int）场次id,（string）分区id));） |
    | concatID   | 联票id     | string | （未开放，请填0）                                |
    | pCode      | 套票id     | string | （未开放，请填0）                                |
    | m          | 供应商id    | string | 查询门票列表的UUaid                             |
    | personID   | 身份证号     | string | 多个请用英文逗号隔开，与ordername配合使用                |
    | memo       | 备注       | string | 可为空                                      |
    - 返回参数说明
    | 参数名称        | 中文名称       | 类型     | 说明              |
    | :---------- | :--------- | :----- | :-------------- |
    | UUordernum  | 票付通订单号     | string |                 |
    | UUremotenum | 远端订单号      | string | 同接收的订单号         |
    | UUcode      | 凭证码        | string | 请求smsSend为1时才返回 |
    | UUqrcodeURL | 订单详情及二维码地址 | string | 请求smsSend为1时才返回 |
    | UUqrcodeIMG | 二维码图片      | string | 请求smsSend为1时才返回 |
     */
    function order_submit($lid, $tid, $m, $order_code, $tprice, $num, $personID, $playtime, $ordername, $ordertel, $contactTEL, $paymode,
                          $assembly = '', $series = '',
                          $memo = '', $ordermode = 0, $concatID = 0, $pCode = 0)
    {
        $parm = array(
            'lid' => $lid,            // 景区id
            'm' => $m,              // 供应商id
            'tid' => $tid,            // 门票id
            'remotenum' => $order_code,      // 远端订单号
            'concatID' => $concatID,       // 联票id
            'pCode' => $pCode,          // 套票id
            'series' => $series,         // 团号

            'personID' => $personID,       // 身份证号
            'tprice' => $tprice,         // 结算价
            'tnum' => $num,           // 数量
            'playtime' => date('Y-m-d', $playtime),       // 游玩日期
            'ordername' => $ordername,      // 取票人姓名
            'ordertel' => $ordertel,       // 取票人手机号
            'contactTEL' => $contactTEL,     // 联系人手机号
            'smsSend' => 1,        // 是否需要发送短信

            'paymode' => $paymode,        // 扣款方式
            'ordermode' => $ordermode,      // 下单方式
            'assembly' => $assembly,       // 集合地点
            'memo' => $memo,           // 备注
        );
        $req = $this->call_function(ActionName::ORDER_SUBMIT, $parm);
        $req_info = $this->verify($req);

        if (!$req_info['success']) {
            $this->request['success'] = 0;
            $this->request['info'] = $req_info['info'];
            return $this->request;
        }

        $order_info = $req_info['info'];

        $arr['ordernum'] = $order_info['UUordernum'];    // 票付通订单号
        $arr['remotenum'] = $order_info['UUremotenum'];    // 远端订单号
        $arr['code'] = $order_info['UUcode'];    // 凭证码              请求smsSend为1时才返回
        $arr['qrcodeURL'] = $order_info['UUqrcodeURL'];    // 订单详情及二维码地址  请求smsSend为1时才返回
        $arr['qrcodeIMG'] = $order_info['UUqrcodeIMG'];    // 二维码图片           请求smsSend为1时才返回

        $this->request['success'] = 1;
        $this->request['info'] = $arr;

        return $this->request;
    }

    /*
     * 修改 取消订单
        | ordern   | 票付通订单号 | string |                              |
        | num      | 剩余数量   | string | 0 为取消订单  -1 不做修改，指要修改订单取票人手机 |
        | ordertel | 取票人手机号 | string | 可为空                          |
        | m        | 预留参数   | string | 可为空
     */
    function change_order($order_num, $num, $ordertel = '', $m = '')
    {
        $parm = array('ordern' => $order_num, 'num' => $num, 'ordertel' => $ordertel, 'm' => $m);
        $req = $this->call_function(ActionName::ORDER_CHANGE, $parm);
        $_req = $req['info']['Rec'];
        if ($_req['UUerrorcode'] == '1097') {
            $this->request['success'] = 0;
            $this->request['info'] = $_req['UUerrorinfo'];
            return $this->request;
        }
        $arr = array();
        $arr['done'] = $_req['UUdone'];              // 查询返回数据 100为成功，无需审核；1095为需要退票审核，等待审核 |
        $arr['fee'] = $_req['UUrefund_fee'];         // 退票扣除的手续费
        $arr['amount'] = $_req['UUrefund_amount'];   //退款购买的单价*退票数量
        $arr['code'] = $_req['UUerrorcode'];
        $arr['info'] = $_req['UUerrorinfo'];

        $this->request['success'] = 1;
        $this->request['info'] = $arr;

        return $this->request;
    }

    function get_error()
    {
        return $this->error;
    }

    // 调用方法
    function call_function($str_function_name, $parm, $is_list = false)
    {
        $req = array('success' => 0, 'info' => '');
        if (!$parm) {
            $req['info'] = '调用方法的 参数 不能为空';
            return $req;
        }
        $data = array_merge(array("ac" => $this->account, "pw" => $this->secret), $parm);

        vendor('nusoap.src.nusoap');
        $soap = new nusoap_client ($this->url, true);
        $soap->soap_defencoding = "utf-8";
        $soap->decode_utf8 = false;
        $soap->xml_encoding = 'utf-8';

        $result = $soap->call($str_function_name, $data);
        $error = $soap->getError();
        if ($error)
            $req['info'] = $error;
        else {
            $req['success'] = 1;
            if ($is_list) {
                if (sizeof(simplexml_load_string($result)) == 1) {
                    $req['info'] = array();
                    $req['info']['Rec'] = array();
                    $req['info']['Rec'][0] = xml2Array($result)['Rec'];
                } else {
                    $req['info'] = xml2Array($result);
                }
            } else {
                $req['info'] = xml2Array($result);
            }
        }
        return $req;
    }

    function verity_parm($parm, $msg = '缺少参数')
    {
        if (!isset($parm) or empty($parm) or $parm == '' or $parm == null) {
            $this->error = $msg;
            return false;
        }
        return true;
    }

// 1 在售，2 下架，3 删除
    static function get_status($status)
    {
        switch ($status) {
            case 1:
                $req = '在售';
                break;
            case 2:
                $req = '下架';
                break;
            case 3:
                $req = '删除';
                break;
        }
        return $req;
    }

    static function get_p_type($p_type)
    {
        switch ($p_type) {
            case 'A':
                $req = Ptype::A;
                break;
            case 'B':
                $req = Ptype::B;
                break;
            case 'C':
                $req = Ptype::C;
                break;
            case 'F':
                $req = Ptype::F;
                break;
            default:
                $req = '找不到产品类型';
                break;
        }
        return $req;
    }

    private function verify($req, $str_key = 'Rec')
    {
        $success = $req['success'];
        if (!$success) {
            $this->error = $req['info'];
            $this->request['success'] = 0;
            $this->request['info'] = $this->error;
            return $this->request;
        }

        $info = $req['info'];
        $error = $info['Rec']['UUerrorcode'];
        if ($error) {
            $this->error = $info['Rec']['UUerrorinfo'];
            $this->request['success'] = 0;
            $this->request['info'] = $this->error;
            return $this->request;
        }
        $this->request['success'] = 1;
        $this->request['info'] = $req['info'][$str_key];
        return $this->request;
    }

    // 生成订单号
    static function getOrderCode()
    {
        return date('YmdHis', time()) . random_string(4);
    }

    /*
     * 生成子订单号
     * $order_code  主订单号
     * $num         订单号 后 随机几位数字 | 0 为再次生成订单号
    */
    function getSubOrderCode($order_code, $num = 3)
    {
        if (!$order_code)
            return '必须 - 主订单号';
        if ($num != 0)
            return $order_code . '-' . random_string($num);
        else
            return $this->getOrderCode();
    }
}

//将XML转为array
function xml2Array($xml)
{
    $values = json_decode(json_encode(simplexml_load_string($xml)), true);
    return $values;
}

//数组转XML
function array2Xml($arr)
{
    $xml = " < xml>";
    foreach ($arr as $key => $val) {
        if (is_numeric($val)) {
            $xml .= " < " . $key . ">" . $val . " </" . $key . " > ";
        } else {
            $xml .= "<" . $key . " ><![CDATA[" . $val . "]] ></" . $key . " > ";
        }
    }
    $xml .= "</xml >";
    return $xml;
}
