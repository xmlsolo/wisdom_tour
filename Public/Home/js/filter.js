$(function () {
    $('.filter-title').click(function () {
        let filter_index = $(this).index();
        let this_filter_box = $('.filterbox').eq(filter_index);
        if (this_filter_box.is(':hidden')) {
            // 隐藏筛选 子菜单
            $('.filterbox').hide();
            // 隐藏底部 弹出子菜单
            if($('.footer'))
                $('.footer .layui-nav-child').hide();
            // 隐藏mask
            mask_action('show');
            this_filter_box.show();
            // 旋转 icon
            $('span.icon').rotate(0);
            let icon = $(this).find('span.icon');
            icon.rotate(180);
        } else {
            // 隐藏筛选 子菜单
            $('.filterbox').hide();
            // 隐藏底部 弹出子菜单
            if($('.footer'))
                $('.footer .layui-nav-child').hide();
            // 隐藏mask
            mask_action('hide');
            this_filter_box.hide();
            // 旋转 icon
            $('span.icon').rotate(0);
        }
    });

    let mask = $('#mask');
    mask.click(function () {
        mask_action('hide');
        $('.filterbox').hide();
        if($('.footer'))
            $('.footer .layui-nav-child').hide();
        // 旋转 icon
        $('span.icon').rotate(0)
    })
});
