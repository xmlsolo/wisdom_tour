$(function () {
    var step1 = $('#step1');
    var step2 = $('#step2');
    var step3 = $('#step3');

    var step1_next = step1.find('.next');

    var step2_prev = step2.find('.prev');
    var step2_next = step2.find('.next');

    var step3_prev = step3.find('.prev');

    step1_next.click(function () {
        step1.hide();
        step3.show();
    });

    step3_prev.click(function () {
        step1.show();
        step3.hide();
    })
});