let date = new Date();
let year = date.getFullYear();
let month = date.getMonth() + 1;

function get_date_num(year, month) {
    $('#month').html(year + '年' + formate_day(month));
    let d = new Date(year, month, 0);
    return d.getDate();
}

function formate_day(day) {
    if (day < 10)
        return '0' + day;
    else
        return day;
}

function change_playtime(ele) {
    let val = ele.attr('data-date');
    $('input[name=playtime]').val(val);
    $('.tickte_list .ticket').each(function (i, item) {
        let date = $(item).attr('data-date');
        if (date === val) {
            $('#playtime').hide();
            $('#playdate').show();
            $(item).find('.check_box').addClass('checked');
        } else {
            $('#playtime').html(val);
            $('#playtime').show();
            $('#playdate').hide();
            $(item).find('.check_box').removeClass('checked');
        }
    });
    let mask = $('#mask');
    $('.datelay').fadeOut('fast');
    mask.hide();
}

function table_date(year, month, days_num, info) {
    let block_td = '<td><span></span></td>';
    let arr_date = [];
    let _arr = [];

    for (let j = 0; j < info.length; j++) {
        _arr.push(info[j]['date'].substr(8, 2));
    }

    let now = new Date();
    let now_date = now.getFullYear() + '-' + (now.getDay() + 1) + '-' + now.getDate();

    for (let i = 0; i < days_num; i++) {
        let d = new Date(year, month - 1, i);
        let str = '';

        let current = '<td>';
        let index = _arr.indexOf("" + formate_day(i + 1));
        let _data = String(year) + "-" + String(formate_day(month)) + "-" + String(formate_day(i + 1));
        if (index !== -1) {
            str = Number(info[index]['retail_price']) / 100;
            current = '<td class="has" data-date="' + _data + '" onclick="change_playtime($(this))">';
            let loop_date = year + '-' + month + '-' + (i + 1);
            if (loop_date === now_date) {
                current = '<td class="current">';
            }
        }

        let req = [];
        req['date'] = i + 1;
        req['str'] = str;
        req['week'] = d.getDay() + 1;

        req['html'] = current + '<span>' + formate_day(i + 1) + '</span><span class="price">' + str + '</span></td>';
        arr_date.push(req)
    }
    let first = arr_date[0];
    let last = arr_date[arr_date.length - 1];
    let first_week_offer = first['week'];
    let last_week_offer = 7 - last['week'];

    if (first_week_offer !== 7) {
        // 补全 月初 之前
        for (let i = 0; i < first_week_offer; i++) {
            let _arr = [];
            _arr['date'] = '';
            _arr['str'] = '';
            _arr['week'] = i;
            _arr['html'] = block_td;
            arr_date.unshift(_arr)
        }
    }
    // 补全 月末 之后
    for (let i = 0; i < last_week_offer - 1; i++) {
        let _arr = [];
        _arr['date'] = '';
        _arr['str'] = '';
        _arr['week'] = i + 1;
        _arr['html'] = block_td;
        arr_date.push(_arr)
    }

    if (last_week_offer === 0) {
        for (let i = 0; i < 7 - 1; i++) {
            let _arr = [];
            _arr['date'] = '';
            _arr['str'] = '';
            _arr['week'] = i + 1;
            _arr['html'] = block_td;
            arr_date.push(_arr)
        }
    }

    let head = '';
    let foot = '';
    let end_html = '';
    for (let i = 0; i < arr_date.length; i++) {
        let data = arr_date[i];
        let week = data['week'];
        let yu = i % 7;
        if (yu === 0) {
            head = '<tr>';
        } else {
            head = '';
        }
        if (yu === 6) {
            foot = '</tr>';
        } else {
            foot = '';
        }
        end_html += head + data['html'] + foot;
    }
    $('table.date #date_body').html(end_html)
}

function change_month(ele, offset) {
    month += offset;
    if (month <= 0) {
        month = 12;
        year -= 1;
    }
    if (month > 12) {
        month = 1;
        year += 1;
    }

    let aid = ele.attr('data-aid');
    let pid = ele.attr('data-pid');
    let url = '/index.php/Home/Piaofutong/ajax_get_today_to_endmouth_all_price';
    let date = new Date();
    let start_date = '';
    let end_date = year + '-' + formate_day(month) + '-' + get_date_num(year, month);
    if (date.getFullYear() + '-' + (date.getMonth() + 1) !== year + '-' + month) {
        start_date = year + '-' + month + '-' + 1;
    }
    let data = {
        'aid': aid,
        'pid': pid,
        'start_date': start_date,
        'end_date': end_date
    };

    $.ajax({
        url: url,
        data: data,
        type: "get",
        success: function (req) {
            table_date(year, month, get_date_num(year, month), req.info.info)
        },
        error: function (req) {

        },
        beforeSend: function () {
            $('table.date #date_body').html('')
        }
    });
}

$(function () {
    let mask = $('#mask');
    $('.show_date_lay').click(function () {
        let aid = $(this).attr('data-aid');
        let pid = $(this).attr('data-pid');
        let url = '/index.php/Home/Piaofutong/ajax_get_today_to_endmouth_all_price';

        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() + 1;


        let end_date = year + '-' + formate_day(month) + '-' + get_date_num(year, month);
        let data = {
            'aid': aid,
            'pid': pid,
            'end_date': end_date
        };
        let display = !$('.datelay').is(':hidden');

        if (display) {
            $('.datelay').fadeOut('fast');
            mask.hide();
        } else {
            $('.datelay').fadeIn('fast');
            mask.show();
        }

        $.ajax({
            url: url,
            data: data,
            type: "get",
            success: function (req) {
                table_date(year, month, get_date_num(year, month), req.info.info)
            },
            error: function (req) {

            },
            beforeSend: function () {
                $('table.date #date_body').html('')
            }
        });
    });
    mask.click(function () {
        $(this).hide();
        $('.datelay').fadeOut('fast');
    });
});