function mask_action(type= 'show',color = "rgba(0,0,0,.8)") {
    let mask = $('#mask');
    if(type === 'show'){
        mask.css('background',color);
        mask.show();
    }else{
        mask.hide();
    }
}
