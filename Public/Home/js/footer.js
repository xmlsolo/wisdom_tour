$(function () {
    let footer = $('.footer');
    let item = footer.find('li.layui-nav-item a');
    item.click(function () {
        let href = $(this).attr('href');
        if(href === "javascript:;"){
            let child = $(this).parent().find('dl');
            if(child.is(':hidden')){
                child.show();
                if($('.filterbox'))
                    $('.filterbox').hide();
                mask_action('show')
                // 旋转 icon
                if($('.filter'))
                    $('span.icon').rotate(0)
            }else{
                child.hide();
                if($('.filterbox'))
                    $('.filterbox').hide();
                mask_action('hide')
                // 旋转 icon
                if($('.filter'))
                    $('span.icon').rotate(0)
            }
        }
    });

    let mask = $('#mask');
    mask.click(function () {
        $('.layui-nav-child').hide();
        if($('.filterbox'))
            $('.filterbox').hide();
        mask_action('hide');
        // 旋转 icon
        if($('.filter'))
            $('span.icon').rotate(0)
    })
});