let total_price = 0;

function change_total_price() {
    total_price = 0;
    $('.price_text').each(function (i, item) {
        let num = Number($(item).parent().parent().parent().find('.num_box input').val());
        if (num < 0) {
            $(item).parent().parent().parent().find('.num_box input').val(0);
        }
        let val = Number($(item).html());
        total_price += num * val;
    });
    if (total_price <= 0) {
        $('#total_price').html(0)
    } else {
        $('#total_price').html(total_price.toFixed(2))
    }
}


$(function () {
    let reduce = $('.num_box .reduce');
    let increase = $('.num_box .increase');
    reduce.click(function () {
        let input = $(this).parent().find('.input input');
        let val = input.val();
        val--;
        if (val <= 0) {
            val = 0;
        }
        input.val(val);
        change_total_price();
    });

    increase.click(function () {
        let input = $(this).parent().find('.input input');
        let val = input.val();
        val++;
        input.val(val);
        change_total_price();
    });

    $('.tickte_list .check_box').click(function () {
        let date = $(this).parent().attr('data-date');
        $('input[name=playtime]').val(date);
        $('.tickte_list .check_box').removeClass('checked');
        $(this).addClass('checked');
    });

    $('.num_box input').change(function () {
        change_total_price();
    });

    change_total_price()
});