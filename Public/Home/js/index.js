layui.use(['layer','flow'],function () {
    let flow = layui.flow;
    let vm = new Vue({
        el: '#list',
        data: {items: []}
    });

    // flow.lazyimg();
    flow.load({
        elem: '.body_container',
        // isAuto : false,
        end :'没有更多',
        done: function(page, next){
            let lis = [];
            let url = "/index.php/Home/Index/ajax_get_seller_list";
            let data = {"p":page};
            $.get({
                url:url,
                data:data,
                type:'get',
                success :function (res) {
                    layui.each(res.info.list,function (index,item) {
                        vm.items.push(item);
                    });
                    next(lis.join(''), page < res.info.pages);
                }
            });
        }
    });
});